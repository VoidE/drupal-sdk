/* eslint-disable max-nested-callbacks */
// tslint:disable
import * as chai from 'chai';
import * as sinonChai from 'sinon-chai';
import * as sinon from 'sinon';
import * as chaiAsPromised from 'chai-as-promised';

import { mockStorage } from './mock/storage';
import { API, APIInterface } from '../src/API';
import { Configuration, ConfigurationInterface } from '../src/Configuration';

const { expect } = chai;
chai.use(sinonChai);

chai.use(chaiAsPromised);

describe('API', () => {
  let api: APIInterface;
  let config: ConfigurationInterface;
  let fetchStub: sinon.SinonStub;

  describe('csrfToken', () => {
    const fakeStorage = mockStorage({});
    beforeEach(() => {
      config = new Configuration({
        url: 'https://drupal-sdk.voide.dev',
        authMode: 'cookie',
        storage: fakeStorage,
      });

      api = new API(config);
    });

    it('set and get csrf Token', () => {
      api.setCSRF('customCSRFToken');

      expect(api.getCSRF()).to.equal('customCSRFToken');
    });

    it('remove csrf Token', () => {
      api.removeCSRF();

      expect(api.getCSRF()).to.equal('');
    });
  });

  describe('csrfToken without store', () => {
    beforeEach(() => {
      config = new Configuration({
        url: 'https://drupal-sdk.voide.dev',
        authMode: 'cookie',
      });

      api = new API(config);
    });

    it('set and get csrf Token', () => {
      api.setCSRF('customCSRFToken');

      expect(api.getCSRF()).to.equal('customCSRFToken');
    });

    it('remove csrf Token', () => {
      api.removeCSRF();

      expect(api.getCSRF()).to.equal('');
    });
  });

  describe('requestCookie', () => {
    beforeEach(() => {
      const fakeStorage = mockStorage({});
      config = new Configuration({
        url: 'https://drupal-sdk.voide.dev',
        authMode: 'cookie',
        storage: fakeStorage,
      });

      api = new API(config);
    });

    it('set and get reqeust cookie', () => {
      api.setRequestCookie('customRequestCookie');

      expect(api.getRequestCookie()).to.equal('customRequestCookie');
    });
  });

  describe('Request Methods ', () => {
    beforeEach(() => {
      const fakeStorage = mockStorage({});
      config = new Configuration({
        url: 'https://drupal-sdk.voide.dev',
        authMode: 'cookie',
        storage: fakeStorage,
      });

      api = new API(config);

      sinon.stub(api, 'request');
    });
    afterEach(() => {
      (api.request as any).restore();
    });

    it('#get()', async () => {
      api.get('/endpoint', { lorem: 'ipsum' });

      expect(api.request).to.have.been.calledWith('get', '/endpoint', {
        lorem: 'ipsum',
      });
    });

    it('#get() without params', async () => {
      api.get('/endpoint');

      expect(api.request).to.have.been.calledWith('get', '/endpoint', {});
    });

    it('#post()', async () => {
      api.post('/endpoint', { lorem: 'ipsum' }, { _format: 'json' });

      expect(api.request).to.have.been.calledWith(
        'post',
        '/endpoint',
        { _format: 'json' },
        {
          lorem: 'ipsum',
        },
      );
    });

    it('#post()', async () => {
      api.post('/endpoint', { lorem: 'ipsum' });

      expect(api.request).to.have.been.calledWith(
        'post',
        '/endpoint',
        {},
        {
          lorem: 'ipsum',
        },
      );
    });

    it('#patch()', async () => {
      api.patch('/endpoint', { lorem: 'ipsum' }, { _format: 'json' });

      expect(api.request).to.have.been.calledWith(
        'patch',
        '/endpoint',
        { _format: 'json' },
        {
          lorem: 'ipsum',
        },
      );
    });

    it('#patch() without params', async () => {
      api.patch('/endpoint', { lorem: 'ipsum' });

      expect(api.request).to.have.been.calledWith(
        'patch',
        '/endpoint',
        {},
        {
          lorem: 'ipsum',
        },
      );
    });

    it('#put()', async () => {
      api.put('/endpoint', { lorem: 'ipsum' }, { _format: 'json' });

      expect(api.request).to.have.been.calledWith(
        'put',
        '/endpoint',
        { _format: 'json' },
        {
          lorem: 'ipsum',
        },
      );
    });

    it('#put() without params', async () => {
      api.put('/endpoint', { lorem: 'ipsum' });

      expect(api.request).to.have.been.calledWith(
        'put',
        '/endpoint',
        {},
        {
          lorem: 'ipsum',
        },
      );
    });

    it('#delete()', async () => {
      await api.delete('/endpoint');

      expect(api.request).to.have.been.calledWith('delete', '/endpoint');
    });
  });

  describe('#request()', () => {
    beforeEach(() => {
      const fakeStorage = mockStorage({});
      config = new Configuration({
        url: 'https://drupal-sdk.voide.dev',
        authMode: 'cookie',
        storage: fakeStorage,
      });

      api = new API(config);
      fetchStub = sinon.stub(global, 'fetch');
      fetchStub.resolves({
        json: () => Promise.resolve({}),
        headers: new Headers(),
      });
    });

    afterEach(() => {
      fetchStub.restore();
    });

    describe('Allows arrays and objects for data', () => {
      it('Does not error when body is an array or object', () => {
        expect(async () => {
          try {
            await api.request('post', '/items', {}, []);
          } catch (err) {
            // error allowed, it only will give us "Network Error" because we mock the calls
          }
        }).to.not.throw();
        expect(async () => {
          try {
            await api.request('post', '/items', {}, {});
          } catch (err) {
            // error allowed, it only will give us "Network Error" because we mock the calls
          }
        }).to.not.throw();
      });
    });

    it('Errors when there is no API URL set', async () => {
      // Set the API URL to undefined
      config.url = undefined;

      // Attempt to make a request and expect it to be rejected with an Error
      await expect(api.request('GET', '/items')).to.be.rejectedWith(
        Error,
        'Drupal SDK has no URL configured to send requests to.',
      );
    });

    it('Calls Axios with the right config', async () => {
      fetchStub.resolves({
        json: () => Promise.resolve({}),
        headers: new Headers(),
      });

      await api.request('get', '/endpoint');

      expect(fetchStub).to.have.been.calledWith(
        'https://drupal-sdk.voide.dev/endpoint',
        {
          method: 'get',
          headers: {},
          credentials: 'include',
        },
      );
    });

    it('Calls Axios with the right config (body)', async () => {
      fetchStub.resolves({
        json: () => Promise.resolve({}),
        headers: new Headers(),
      });

      await api.request(
        'post',
        '/endpoint',
        {},
        {
          testing: true,
        },
      );

      expect(fetchStub).to.have.been.calledWith(
        'https://drupal-sdk.voide.dev/endpoint',
        {
          method: 'post',
          headers: {},
          credentials: 'include',
          body: JSON.stringify({ testing: true }),
        },
      );
    });

    it('Calls Axios with the right config (params)', async () => {
      fetchStub.resolves({
        json: () => Promise.resolve({}),
        headers: new Headers(),
      });

      await api.request('get', '/endpoint', { queryParam: true });

      expect(fetchStub).to.have.been.calledWith(
        'https://drupal-sdk.voide.dev/endpoint?queryParam=true',
        {
          method: 'get',
          headers: {},
          credentials: 'include',
        },
      );
    });

    it('Calls Axios with the right config (params)', async () => {
      fetchStub.resolves({
        json: () => Promise.resolve({}),
        headers: new Headers(),
      });

      await api.request('get', '/endpoint', { queryParam: true });

      expect(fetchStub).to.have.been.calledWith(
        'https://drupal-sdk.voide.dev/endpoint?queryParam=true',
        {
          method: 'get',
          headers: {},
          credentials: 'include',
        },
      );
    });

    it('Calls Axios with the right config (headers)', async () => {
      await api.request('get', '/jsonapi/node/article', { queryParam: true });

      expect(fetchStub).to.have.been.calledWith(
        'https://drupal-sdk.voide.dev/jsonapi/node/article?queryParam=true',
        {
          method: 'get',
          headers: { 'Content-Type': 'application/vnd.api+json' },
          credentials: 'include',
        },
      );
    });

    it('Calls Axios with the right config (headers)', async () => {
      fetchStub.resolves({
        json: () => Promise.resolve({}),
        headers: new Headers(),
      });

      await api.request('get', 'en/jsonapi/node/article', { queryParam: true });

      expect(fetchStub).to.have.been.calledWith(
        'https://drupal-sdk.voide.dev/en/jsonapi/node/article?queryParam=true',
        {
          method: 'get',
          headers: { 'Content-Type': 'application/vnd.api+json' },
          credentials: 'include',
        },
      );
    });

    it('Add extra options to the fetch request', async () => {
      const fakeStorage = mockStorage({});
      const scopedConfig = new Configuration({
        url: 'https://drupal-sdk.voide.com/',
        storage: fakeStorage,
      });

      const scopedApi = new API(scopedConfig);
      await scopedApi.get(
        '/endpoint',
        {},
        {
          next: { revalidate: 0 },
        },
      );

      expect(fetchStub).to.have.been.calledWith(
        'https://drupal-sdk.voide.com/endpoint',
        {
          method: 'get',
          headers: {},
          credentials: 'include',
          next: { revalidate: 0 },
        },
      );
    });

    it('Reformat base url', async () => {
      const fakeStorage = mockStorage({});
      const scopedConfig = new Configuration({
        url: 'https://drupal-sdk.voide.com/',
        storage: fakeStorage,
      });

      const scopedApi = new API(scopedConfig);
      await scopedApi.request('get', '/endpoint');

      expect(fetchStub).to.have.been.calledWith(
        'https://drupal-sdk.voide.com/endpoint',
        {
          method: 'get',
          headers: {},
          credentials: 'include',
        },
      );
    });

    it('Adds Bearer header if access token is set', async () => {
      config.token = 'CustomTokenString';

      await api.request('get', '/endpoint', { queryParam: true });
      expect(fetchStub).to.have.been.calledWith(
        'https://drupal-sdk.voide.dev/endpoint?queryParam=true',
        {
          method: 'get',
          headers: {
            Authorization: `Bearer ${config.token}`,
          },
          credentials: 'include',
        },
      );
    });

    it('Adds CSRF token header if token is set', async () => {
      fetchStub.resolves({
        json: () => Promise.resolve({}),
        headers: new Headers(),
      });

      api.setCSRF('customCSRFToken');

      await api.request('get', '/endpoint', { queryParam: true });

      expect(fetchStub).to.have.been.calledWith(
        'https://drupal-sdk.voide.dev/endpoint?queryParam=true',
        {
          method: 'get',
          headers: {
            'X-CSRF-TOKEN': 'customCSRFToken',
          },
          credentials: 'include',
        },
      );
    });

    it('Adds CSRF token header if token is set', async () => {
      const fakeStorage = mockStorage({});
      const scopedConfig = new Configuration({
        url: 'https://drupal-sdk.voide.dev',
        authMode: 'jwt',
        storage: fakeStorage,
      });

      const scopedApi = new API(scopedConfig);
      scopedApi.setCSRF('customCSRFToken');

      await scopedApi.request('get', '/endpoint', { queryParam: true });

      expect(fetchStub).to.have.been.calledWith(
        'https://drupal-sdk.voide.dev/endpoint?queryParam=true',
        {
          method: 'get',
          headers: {},
          credentials: 'include',
        },
      );
    });

    it('Adds basic auth if set', async () => {
      const fakeStorage = mockStorage({});
      const scopedConfig = new Configuration({
        url: 'https://drupal-sdk.voide.dev/',
        storage: fakeStorage,
        basicAuth: {
          username: 'basicAuthUserName',
          password: 'basicAuthPassword',
        },
      });

      const scopedApi = new API(scopedConfig);

      await scopedApi.request('get', '/endpoint');

      expect(fetchStub).to.have.been.calledWith(
        'https://drupal-sdk.voide.dev/endpoint',
        {
          headers: {
            Authorization:
              'Basic YmFzaWNBdXRoVXNlck5hbWU6YmFzaWNBdXRoUGFzc3dvcmQ=',
          },
          method: 'get',
          credentials: 'include',
        },
      );
    });

    it('Adds request cookie if set', async () => {
      fetchStub.resolves({
        json: () => Promise.resolve({}),
        headers: new Headers(),
      });

      api.setRequestCookie('customRequestCookie');

      await api.request('get', '/endpoint', { queryParam: true });

      expect(fetchStub).to.have.been.calledWith(
        'https://drupal-sdk.voide.dev/endpoint?queryParam=true',
        {
          headers: {
            Cookie: 'customRequestCookie',
          },
          method: 'get',
          credentials: 'include',
        },
      );
    });

    it('Check for request cookie', async () => {
      fetchStub.returns(
        Promise.resolve({
          headers: new Headers({
            'set-cookie': 'customSetCookie',
          }),
          json: () => Promise.resolve({}),
        }),
      );

      await api.request('get', '/endpoint', { queryParam: true });

      expect(api.getRequestCookie()).to.equal('customSetCookie');
    });

    describe('Hooks', () => {
      it('Reformat base url', async () => {
        const scopedConfig = new Configuration({
          url: 'https://drupal-sdk.voide.dev/',
          hooks: {
            beforeRequest: [
              async ({ requestOptions, ...args }) => ({
                ...args,
                requestOptions: {
                  ...requestOptions,
                  baseURL: undefined,
                  url: `/api/drupal${requestOptions.url}`,
                },
              }),
            ],
            beforePathRequest: [
              async ({ data, params, ...args }) => {
                const includes = [
                  'field_paragraphs',
                  'field_paragraphs.field_media',
                  'field_paragraphs.field_media.field_media_image',
                  'field_header_image.field_media_image',
                  'field_paragraphs.field_items',
                  'field_paragraphs.field_items.field_media',
                  'field_paragraphs.field_items.field_media.field_media_image',
                ];

                const paragraphResources = ['node--landing_page'];
                if (paragraphResources.includes(data.jsonapi.resourceName)) {
                  params.include = includes.join(',');
                }

                return {
                  ...args,
                  params,
                  data,
                };
              },
            ],
          },
        });

        const scopedApi = new API(scopedConfig);

        await scopedApi.request('get', '/endpoint');

        expect(fetchStub).to.have.been.calledWith(
          'https://drupal-sdk.voide.dev/api/drupal/endpoint',
          {
            headers: {},
            method: 'get',
            credentials: 'include',
          },
        );
      });
    });
    describe('Helpers', () => {
      it('#setHeader()', async () => {
        const scopedConfig = new Configuration({
          url: 'https://drupal-sdk.voide.dev/',
        });

        const scopedApi = new API(scopedConfig);

        scopedApi.setHeader('X-Custom-Header', 'Lorem ipsum');
        await scopedApi.request('get', '/endpoint');

        expect(fetchStub).to.have.been.calledWith(
          'https://drupal-sdk.voide.dev/endpoint',
          {
            headers: {
              'X-Custom-Header': 'Lorem ipsum',
            },
            method: 'get',
            credentials: 'include',
          },
        );
      });
    });

    describe('Helpers', () => {
      it('#setHeader()', async () => {
        const scopedConfig = new Configuration({
          url: 'https://drupal-sdk.voide.dev/',
        });

        const scopedApi = new API(scopedConfig);

        scopedApi.setHeader('X-Custom-Header', 'Lorem ipsum');
        await scopedApi.request('get', '/endpoint');

        expect(fetchStub).to.have.been.calledWith(
          'https://drupal-sdk.voide.dev/endpoint',
          {
            headers: {
              'X-Custom-Header': 'Lorem ipsum',
            },
            method: 'get',
            credentials: 'include',
          },
        );
      });
    });
  });

  describe('#request() - Errors', () => {
    beforeEach(() => {
      const fakeStorage = mockStorage({});
      config = new Configuration({
        url: 'https://drupal-sdk.voide.dev',
        authMode: 'cookie',
        storage: fakeStorage,
      });

      api = new API(config);
      fetchStub = sinon.stub(global, 'fetch');
    });

    afterEach(() => {
      fetchStub.restore();
    });

    it('Returns network error if the API did not respond', async () => {
      fetchStub.rejects({
        status: 404,
        json: () =>
          Promise.resolve({
            message: 'Not Found',
          }),
      });

      try {
        await api.request('get', '/endpoint');
      } catch (err) {
        const error = err;
        expect(error.message).to.equal('Not Found');
        expect(error.code).to.equal('404');
        expect(error.data).to.deep.equal({
          message: 'Not Found',
        });
        expect(error.violations).to.deep.equal([]);
      }
    });

    it('Returns network error with responseUrl if the API did not respond', async () => {
      fetchStub.rejects({
        status: 404,
        json: () =>
          Promise.resolve({
            message: 'Not Found',
          }),
        request: {
          res: {
            responseUrl: 'https://drupal-sdk.voide.dev/endpoint',
          },
        },
      });

      try {
        await api.request('get', '/endpoint');
      } catch (err) {
        const error = err;
        expect(error.responseUrl).to.equal(
          'https://drupal-sdk.voide.dev/endpoint',
        );
      }
    });

    it("Returns unprocessable entity error if there's an invalid JSONAPI input", async () => {
      fetchStub.rejects({
        status: 422,
        json: () =>
          Promise.resolve({
            jsonapi: {
              version: '1.0',
              meta: {
                links: {
                  self: {
                    href: 'http://jsonapi.org/format/1.0/',
                  },
                },
              },
            },
            errors: [
              {
                title: 'Unprocessable Content',
                status: '422',
                detail:
                  'body.0.format: The value you selected is not a valid choice.',
                source: {
                  pointer: '/data/attributes/body.0.format',
                },
              },
              {
                title: 'Unprocessable Content',
                status: '422',
                detail: "I'm a teapot",
                source: {
                  pointer: '/data/attributes/id',
                },
              },
            ],
          }),
        headers: new Headers(),
      });

      try {
        await api.request('post', '/jsonapi/node/vacancy');
      } catch (err) {
        const error = err;
        expect(error.message).to.equal('Unprocessable Content');
        expect(error.violations).to.deep.equal([
          {
            detail:
              'body.0.format: The value you selected is not a valid choice.',
            fieldName: 'body.0.format',
            message: 'The value you selected is not a valid choice.',
          },
          {
            detail: "I'm a teapot",
          },
        ]);
        expect(error.code).to.equal('422');
      }
    });
    it('should handle non-api errors correctly', async () => {
      // Mock fetch to simulate a network error
      fetchStub.rejects(new TypeError('Syntax error anywhere'));

      try {
        await api.request('get', '/jsonapi/node/vacancy');
      } catch (err) {
        const error = err;
        expect(error.message).to.equal('Syntax error anywhere');
        expect(error.code).to.equal(undefined);
        expect(error.url).to.equal(undefined);
      }
    });

    it('should handle network errors correctly', async () => {
      // Mock fetch to simulate a network error
      fetchStub.rejects({
        status: -1,
        json: () => Promise.resolve(''),
      });

      try {
        await api.request('get', '/jsonapi/node/vacancy');
      } catch (err) {
        const error = err;
        expect(error.message).to.equal('Unknown error occured');
        expect(error.code).to.equal('-1');
        expect(error.url).to.equal('/jsonapi/node/vacancy');
      }
    });
  });

  // describe('Errors', () => {
  //   beforeEach(() => {
  //     const fakeStorage = mockStorage({});
  //     config = new Configuration({
  //       url: 'https://drupal-sdk.voide.dev',
  //       authMode: 'cookie',
  //       storage: fakeStorage,
  //       hooks: {
  //         beforeRequest: [
  //           async ({ requestOptions, ...args }) => {
  //             requestOptions.returnRaw = true;
  //             return { ...args, requestOptions };
  //           },
  //         ],
  //       },
  //     });

  //     api = new API(config);
  //   });

  //   it("Returns unprocessable entity error if there's and invalid input", async () => {
  //     sinon.stub(api.xhr, 'request').rejects({
  //       response: {
  //         status: 422,
  //         data: {
  //           errors: [
  //             {
  //               title: 'Unprocessable Entity',
  //               status: 422,
  //               detail:
  //                 'body.0.format: The value you selected is not a valid choice.',
  //               source: {
  //                 pointer: '/data/attributes/body/format',
  //               },
  //             },
  //             {
  //               title: 'Unprocessable Entity',
  //               status: 418,
  //               detail: "I'm a teapot",
  //             },
  //           ],
  //         },
  //       },
  //       request: {},
  //     });

  //     try {
  //       await api.request('get', '/endpoint');
  //     } catch (err) {
  //       const error = err;
  //       expect(error).to.deep.equal({
  //         request: {},
  //         response: {
  //           status: 422,
  //           data: {
  //             errors: [
  //               {
  //                 title: 'Unprocessable Entity',
  //                 status: 422,
  //                 detail:
  //                   'body.0.format: The value you selected is not a valid choice.',
  //                 source: {
  //                   pointer: '/data/attributes/body/format',
  //                 },
  //               },
  //               {
  //                 title: 'Unprocessable Entity',
  //                 status: 418,
  //                 detail: "I'm a teapot",
  //               },
  //             ],
  //           },
  //         },
  //       });
  //     }
  //   });
});
