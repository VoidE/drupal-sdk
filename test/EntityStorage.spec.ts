/* eslint-disable max-nested-callbacks */
import * as chai from 'chai';
import * as sinonChai from 'sinon-chai';
import * as sinon from 'sinon';
import { mockStorage } from './mock/storage';
import { EntityStorage } from '../src/EntityStorage';
import { API } from '../src/API';
import { Configuration } from '../src/Configuration';
import { EntityMappingInterface } from '../src/schemes/mapping';

const { expect } = chai;
chai.use(sinonChai);

describe('EntityStorage Class', () => {
  let Entity: EntityStorage;
  let config: Configuration;
  let api: API;
  let singleResponse: { [key: string]: any };
  let mappedSingleResponse: { [key: string]: any };
  let multipleResponse: { [key: string]: any };
  const fakeStorage = mockStorage({
    name: 'John Doe',
  });

  beforeEach(() => {
    config = new Configuration({
      url: 'https://drupal-sdk.voide.dev',
      authMode: 'cookie',
      storage: fakeStorage,
    });

    api = new API(config);
    Entity = new EntityStorage('node', 'article', { api, config, mapping: {} });
    singleResponse = {
      jsonapi: {
        version: '1.0',
        meta: {
          links: {
            self: {
              href: 'http://jsonapi.org/format/1.0/',
            },
          },
        },
      },
      data: {
        type: 'node--script',
        id: 'f502677c-bfd1-4656-890d-f03af89e6be9',
        attributes: {
          title: 'Lorem 1',
          field_date: {
            value: '2021-10-28T15:00:00+00:00',
            end_value: '2021-10-28T17:00:00+00:00',
          },
          items: [
            { data: { title: 'Lorem 2', type: 'image' } },
            { data: { title: 'Lorem 3', type: 'video' } },
            { data: { title: 'Lorem 4', type: 'image' } },
          ],
        },
      },
    };
    mappedSingleResponse = {
      jsonapi: {
        version: '1.0',
        meta: {
          links: {
            self: {
              href: 'http://jsonapi.org/format/1.0/',
            },
          },
        },
      },
      data: {
        type: 'node--script',
        id: 'f502677c-bfd1-4656-890d-f03af89e6be9',
        attributes: {
          title: 'Lorem 1',
          field_date: {
            value: '2021-10-28T15:00:00+00:00',
            end_value: '2021-10-28T17:00:00+00:00',
          },
          items: [
            { data: { title: 'Lorem 2', type: 'image' } },
            { data: { title: 'Lorem 3', type: 'video' } },
            { data: { title: 'Lorem 4', type: 'image' } },
          ],
        },
      },

      mapped: {
        date: {
          end: '2021-10-28T17:00:00+00:00',
          start: '2021-10-28T15:00:00+00:00',
        },
        items: ['Lorem 2', 'Lorem 3', 'Lorem 4'],
        title: 'Lorem 1',
      },
    };
    multipleResponse = {
      jsonapi: {
        version: '1.0',
        meta: {
          links: {
            self: {
              href: 'http://jsonapi.org/format/1.0/',
            },
          },
        },
      },
      data: [
        {
          type: 'node--script',
          id: 'f502677c-bfd1-4656-890d-f03af89e6be9',
          attributes: {
            title: 'Lorem 1',
            field_date: {
              value: '2021-10-28T15:00:00+00:00',
              end_value: '2021-10-28T17:00:00+00:00',
            },
            items: [
              { data: { title: 'Lorem 2', type: 'image' } },
              { data: { title: 'Lorem 3', type: 'video' } },
              { data: { title: 'Lorem 4', type: 'image' } },
            ],
          },
        },
        {
          type: 'node--script',
          id: 'f502677c-bfd1-4656-890d-f03af89e6be9',
          attributes: {
            title: 'Lorem 1',
            field_date: {
              value: '2021-10-28T15:00:00+00:00',
              end_value: '2021-10-28T17:00:00+00:00',
            },
            items: [
              { data: { title: 'Lorem 2', type: 'image' } },
              { data: { title: 'Lorem 3', type: 'video' } },
              { data: { title: 'Lorem 4', type: 'image' } },
            ],
          },
        },
      ],
    };
  });

  it('#read()', async () => {
    sinon.stub(Entity.api, 'get').resolves(singleResponse);
    await Entity.read('74518cfa-77ad-11eb-9439-0242ac130002');

    expect(Entity.api.get).to.have.been.calledWith(
      '/jsonapi/node/article/74518cfa-77ad-11eb-9439-0242ac130002',
    );
  });

  it('#readAll()', async () => {
    sinon.stub(Entity.api, 'get').returns(Promise.resolve(multipleResponse));

    await Entity.readAll();

    expect(Entity.api.get).to.have.been.calledWith('/jsonapi/node/article');
  });

  it('#readAll()', async () => {
    sinon.stub(Entity.api, 'get').returns(Promise.resolve(multipleResponse));

    await Entity.readAll({ filter: { title: 'Lorem' } });

    expect(Entity.api.get).to.have.been.calledWith('/jsonapi/node/article', {
      filter: { title: 'Lorem' },
    });
  });

  it('#create()', async () => {
    sinon.stub(Entity.api, 'post').resolves(singleResponse);
    Entity.create({ attributes: { title: 'Lorem' } });

    expect(Entity.api.post).to.have.been.calledWith('/jsonapi/node/article', {
      data: {
        type: 'node--article',
        attributes: {
          title: 'Lorem',
        },
      },
    });
  });

  it('#create()', async () => {
    sinon.stub(Entity.api, 'post').resolves(singleResponse);
    Entity.create(
      { attributes: { title: 'Lorem' } },
      { filter: { lorem: 'ipsum' } },
    );

    expect(Entity.api.post).to.have.been.calledWith(
      '/jsonapi/node/article',
      {
        data: {
          type: 'node--article',
          attributes: {
            title: 'Lorem',
          },
        },
      },
      { filter: { lorem: 'ipsum' } },
    );
  });

  it('#create()', async () => {
    sinon.stub(Entity.api, 'post').resolves(singleResponse);
    Entity.create(
      { attributes: { title: 'Lorem' } },
      { filter: { lorem: 'ipsum' } },
    );

    expect(Entity.api.post).to.have.been.calledWith(
      '/jsonapi/node/article',
      {
        data: {
          type: 'node--article',
          attributes: {
            title: 'Lorem',
          },
        },
      },
      { filter: { lorem: 'ipsum' } },
    );
  });

  it('#update()', async () => {
    sinon.stub(Entity.api, 'patch').resolves(singleResponse);
    Entity.update('74518cfa-77ad-11eb-9439-0242ac130002', {
      attributes: { title: 'Lorem' },
    });

    expect(Entity.api.patch).to.have.been.calledWith(
      '/jsonapi/node/article/74518cfa-77ad-11eb-9439-0242ac130002',
      {
        data: {
          type: 'node--article',
          id: '74518cfa-77ad-11eb-9439-0242ac130002',
          attributes: {
            title: 'Lorem',
          },
        },
      },
      {},
    );
  });

  it('#update()', async () => {
    sinon.stub(Entity.api, 'patch').resolves(singleResponse);
    Entity.update(
      '74518cfa-77ad-11eb-9439-0242ac130002',
      { attributes: { title: 'Lorem' } },
      { filter: { lorem: 'ipsum' } },
    );

    expect(Entity.api.patch).to.have.been.calledWith(
      '/jsonapi/node/article/74518cfa-77ad-11eb-9439-0242ac130002',
      {
        data: {
          type: 'node--article',
          id: '74518cfa-77ad-11eb-9439-0242ac130002',
          attributes: {
            title: 'Lorem',
          },
        },
      },
      { filter: { lorem: 'ipsum' } },
    );
  });

  it('#delete()', async () => {
    sinon.stub(Entity.api, 'delete');
    Entity.delete('74518cfa-77ad-11eb-9439-0242ac130002');

    expect(Entity.api.delete).to.have.been.calledWith(
      '/jsonapi/node/article/74518cfa-77ad-11eb-9439-0242ac130002',
    );
  });

  it('#delete()', async () => {
    sinon.stub(Entity.api, 'delete');
    Entity.delete('74518cfa-77ad-11eb-9439-0242ac130002', {
      filter: { lorem: 'ipsum' },
    });

    expect(
      Entity.api.delete,
    ).to.have.been.calledWith(
      '/jsonapi/node/article/74518cfa-77ad-11eb-9439-0242ac130002',
      { filter: { lorem: 'ipsum' } },
    );
  });

  it('#upload()', async () => {
    sinon.stub(Entity.api, 'request');

    (Entity.api.request as any).returns(
      Promise.resolve({
        response: {
          data: {
            lorem: 'ipsum',
          },
        },
      }),
    );

    await Entity.upload('MockFileBinary', 'lorem.txt', 'field_file');

    expect(Entity.api.request).to.have.been.calledWith(
      'post',
      `/jsonapi/node/article/field_file`,
      {},
      'MockFileBinary',
      {
        'Content-Type': 'application/octet-stream',
        Accept: 'application/vnd.api+json',
        'Content-Disposition': `file; filename="lorem.txt"`,
      },
    );
  });

  it('#processParams()', async () => {
    const ScopedEntity = new EntityStorage('node', 'article', {
      api,
      config,
      mapping: {},
    });
    const params = {
      filter: {
        id: 3,
      },
    };

    expect(ScopedEntity.processParams(params)).to.deep.equal({
      bundle: 'article',
      filter: {
        id: 3,
      },
    });
  });

  it('#processParams() with bundle as param', async () => {
    const ScopedEntity = new EntityStorage('node', undefined, {
      api,
      config,
      mapping: {},
    });
    const params = {
      bundle: 'article',
      filter: {
        id: 3,
      },
    };

    expect(ScopedEntity.processParams(params)).to.deep.equal({
      bundle: 'article',
      filter: {
        id: 3,
      },
    });
  });

  it('#processParams() without bundle', async () => {
    const ScopedEntity = new EntityStorage('node', undefined, {
      api,
      config,
      mapping: {},
    });
    const params = {
      filter: {
        id: 3,
      },
    };

    expect(ScopedEntity.processParams(params)).to.deep.equal({
      bundle: 'node',
      filter: {
        id: 3,
      },
    });
  });
});
