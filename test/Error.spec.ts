/* eslint-disable max-nested-callbacks */
// tslint:disable
import * as chai from 'chai';
import * as sinonChai from 'sinon-chai';
import { APIError } from '../src/Error';

const { expect } = chai;
chai.use(sinonChai);

describe('APIStorage', () => {
  describe('accessors', () => {
    let globalError: APIError;

    beforeEach(() => {
      const baseErrorInfo = {
        url: 'https://drupal-sdk.voide.dev',
        method: 'post',
        params: {
          lorem: 'ipsum',
        },
        code: 404,
        data: {
          lorem: 'ipsum',
          dolor: {
            sit: 'amet',
          },
        },
      };

      globalError = new APIError('Page not found', baseErrorInfo);
    });

    describe('#url()', () => {
      it('Get url of the Error object`', () => {
        expect(globalError.url).to.equal('https://drupal-sdk.voide.dev');
      });
    });

    describe('#method()', () => {
      it('Get method of the Error object`', () => {
        expect(globalError.method).to.equal('POST');
      });
    });

    describe('#code()', () => {
      it('Get code of the Error object`', () => {
        expect(globalError.code).to.equal('404');
      });
    });

    describe('#params()', () => {
      it('Get params of the Error object`', () => {
        expect(globalError.params).to.deep.equal({ lorem: 'ipsum' });
      });
    });

    describe('#data()', () => {
      it('Get data of the Error object`', () => {
        expect(globalError.data).to.deep.equal({
          lorem: 'ipsum',
          dolor: {
            sit: 'amet',
          },
        });
      });
    });

    describe('#responseUrl()', () => {
      it('Get responseUrl of the Error object`', () => {
        expect(globalError.responseUrl).to.equal('');
      });
    });

    describe('#toString()', () => {
      it('Get string of the Error object`', () => {
        expect(globalError.toString()).to.equal(
          'Drupal call failed: POST https://drupal-sdk.voide.dev {"lorem":"ipsum"} - Page not found (code 404)',
        );
      });
    });
  });

  describe('accessors with responseUrl', () => {
    let globalError: APIError;

    beforeEach(() => {
      const baseErrorInfo = {
        url: 'https://drupal-sdk.voide.dev',
        responseUrl: 'https://drupal-sdk.voide.dev/lorem-ipsum',
        method: 'post',
        params: {
          lorem: 'ipsum',
        },
        code: 404,
      };

      globalError = new APIError('Page not found', baseErrorInfo);
    });

    describe('#responseUrl()', () => {
      it('Get responseUrl of the Error object`', () => {
        expect(globalError.responseUrl).to.equal(
          'https://drupal-sdk.voide.dev/lorem-ipsum',
        );
      });
    });

    describe('#toString()', () => {
      it('Get string of the Error object`', () => {
        expect(globalError.toString()).to.equal(
          'Drupal call failed: POST https://drupal-sdk.voide.dev {"lorem":"ipsum"} - Page not found (code 404) - Original URL: https://drupal-sdk.voide.dev/lorem-ipsum',
        );
      });
    });
  });

  describe('defaults', () => {
    let globalError: APIError;

    beforeEach(() => {
      const baseErrorInfo = {
        url: 'https://drupal-sdk.voide.dev',
        method: 'post',

        code: 404,
      };

      globalError = new APIError('Unknown error occured', baseErrorInfo);
    });

    describe('#params()', () => {
      it('Get params of the Error object`', () => {
        expect(globalError.params).to.deep.equal({});
      });
    });

    describe('#data()', () => {
      it('Get data of the Error object`', () => {
        expect(globalError.data).to.deep.equal({});
      });
    });
  });
});
