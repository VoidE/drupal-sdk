import * as chai from 'chai';
import * as sinonChai from 'sinon-chai';
import { querify } from '../src/utils/querify';

const { expect } = chai;
chai.use(sinonChai);

describe('queryString', () => {
  describe('#querify()', () => {
    it('Serialize single parameters', () => {
      const queryString = querify({ lorem: 'ipsum' });
      expect(queryString).to.be.equal('lorem=ipsum');
    });

    it('Serialize multiple parameters', () => {
      const queryString = querify({ lorem: 'ipsum', dolor: 'sit' });
      expect(queryString).to.be.equal('lorem=ipsum&dolor=sit');
    });

    it('Serialize array parameters', () => {
      const queryString = querify({ field: ['blaat', 'schaap'] });
      expect(queryString).to.be.equal('field[0]=blaat&field[1]=schaap');
    });

    it('Serialize complex parameters', () => {
      const queryString = querify({ parent: { child: { value: 1 } } });
      expect(queryString).to.be.equal('parent[child][value]=1');
    });

    it('Serialize parameters that needs encoding', () => {
      const queryString = querify({ name: 'lorem & ipsum' });
      expect(queryString).to.be.equal('name=lorem%20%26%20ipsum');
    });
  });
});
