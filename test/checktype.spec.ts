/* eslint-disable max-nested-callbacks */
import * as chai from 'chai';
import * as sinonChai from 'sinon-chai';
import {
  isNotNull,
  isString,
  isNumber,
  isFunction,
  isObjectOrEmpty,
  isArrayOrEmpty,
  isArray,
  isObject,
} from '../src/utils/checkType';

const { expect } = chai;
chai.use(sinonChai);

describe('check types', () => {
  describe('#checktype()', () => {
    it('#isNotNull()', () => {
      expect(isNotNull('lorem')).to.equal(true);
    });
    it('#isNotNull() false', () => {
      expect(isNotNull('lorem')).to.equal(true);
    });

    it('#isString()', () => {
      expect(isString('lorem')).to.equal(true);
    });
    it('#isString() false', () => {
      expect(isString('lorem')).to.equal(true);
    });

    it('#isNumber()', () => {
      expect(isNumber(12323)).to.equal(true);
    });
    it('#isNumber() false', () => {
      expect(isNumber(12323)).to.equal(true);
    });

    it('#isFunction()', () => {
      expect(isFunction(() => {})).to.equal(true);
    });
    it('#isFunction() false', () => {
      expect(isFunction(() => {})).to.equal(true);
    });

    it('#isObjectOrEmpty()', () => {
      expect(isObjectOrEmpty({})).to.equal(true);
    });
    it('#isObjectOrEmpty() false', () => {
      expect(isObjectOrEmpty({})).to.equal(true);
    });

    it('#isArrayOrEmpty()', () => {
      expect(isArrayOrEmpty([])).to.equal(true);
    });
    it('#isArrayOrEmpty() false', () => {
      expect(isArrayOrEmpty([])).to.equal(true);
    });

    it('#isArray()', () => {
      expect(isArray(['lorem', 'ipsum'])).to.equal(true);
    });
    it('#isArray() false', () => {
      expect(isArray('notAnArray')).to.equal(false);
    });

    it('#isObject()', () => {
      expect(
        isObject({
          lorem: 'ipsum',
        }),
      ).to.equal(true);
    });

    it('#isObject()', () => {
      expect(isObject(['ipsum'])).to.equal(false);
    });
  });
});
