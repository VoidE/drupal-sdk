/* eslint-disable max-nested-callbacks */
// tslint:disable
import * as chai from 'chai';
import * as sinonChai from 'sinon-chai';
import { mockStorage, emptyMockStorage } from './mock/storage';
import { APIStorage } from '../src/APIStorage';

const { expect } = chai;
chai.use(sinonChai);

describe('APIStorage', () => {
  describe('Valid storage', () => {
    let Store: APIStorage;
    const fakeStorage = mockStorage({
      name: 'John Doe',
    });

    beforeEach(() => {
      Store = new APIStorage(fakeStorage, 'storage-key');
    });

    describe('#setItem()', () => {
      it('Set item of the APIStorage`', () => {
        Store.setItem('lorem', 'ipsum');
        expect(fakeStorage.values.lorem).to.equal('ipsum');
      });
    });

    describe('#getItem()', () => {
      it('Set item of the APIStorage`', () => {
        const name = Store.getItem('name');
        expect(name).to.equal('John Doe');
      });
    });

    describe('#removeItem()', () => {
      it('Remove item of the APIStorage`', () => {
        Store.removeItem('name');
        expect(fakeStorage.values.name).to.equal(undefined);
      });
    });

    describe('#setItem()', () => {
      const fakeEmptyStorage = mockStorage({});
      const emptyStore = new APIStorage(fakeEmptyStorage, 'empty-storage');
      it('Set item of the APIStorage`', () => {
        emptyStore.setItem('lorem', 'ipsum');
        expect(fakeEmptyStorage.values.lorem).to.equal('ipsum');
      });
    });
  });

  describe('Empty storage', () => {
    let Store: APIStorage;
    const emptyStorage = emptyMockStorage();

    beforeEach(() => {
      Store = new APIStorage(emptyStorage, 'storage-key');
    });

    it('#getItem()', () => {
      const name = Store.getItem('name');
      expect(name).to.equal(undefined);
    });

    it('#setItem`', () => {
      Store.setItem('lorem', 'ipsum');
      expect(emptyStorage.values.lorem).to.equal('ipsum');
    });
  });
});
