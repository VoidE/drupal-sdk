import { APIStorageInterface } from '../../src/APIStorage';

export const mockStorage = <T extends object>(
  fakeStorage: T,
): APIStorageInterface & { values: any & T } => {
  const api = {
    values: fakeStorage,
    getItem() {
      return JSON.stringify(api.values);
    },
    setItem(key: string, values: any) {
      api.values = JSON.parse(values);
    },
  };

  return api as any;
};

export const emptyMockStorage = (): APIStorageInterface & {
  values: any;
} => {
  const api = {
    values: null,
    getItem() {
      return null;
    },
    setItem(key: string, values: any) {
      api.values = JSON.parse(values);
    },
  };

  return api as any;
};
