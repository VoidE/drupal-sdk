/* eslint-disable max-nested-callbacks */
import * as chai from 'chai';
import * as sinonChai from 'sinon-chai';
import * as chaiAsPromised from 'chai-as-promised';
import * as sinon from 'sinon';
import DrupalSDK, { formatDate, formatApiDate } from '../src/index';
import { EntityStorage } from '../src/EntityStorage';
import { APIInterface } from '../src/API';
import { QueryParams as QueryParamsType } from '../src/schemes/http/Query';
import { mockStorage } from './mock/storage';

const { expect } = chai;
chai.use(sinonChai);
chai.use(chaiAsPromised);

describe('Connection Class', () => {
  let fetchStub: any;

  beforeEach(() => {
    fetchStub = sinon.stub(global, 'fetch');

    // @ts-ignore
    fetchStub.resolves({
      json: () => Promise.resolve({}),
      headers: new Headers(),
    });
  });

  afterEach(() => {
    fetchStub.restore();
  });

  it('Create new SDK object without errors', () => {
    const instance = new DrupalSDK(undefined as any);
    expect(() => instance).not.to.throw();
  });

  it('Set apiBasePath on creation', () => {
    const instance = new DrupalSDK({
      url: 'https://drupal-sdk.prod.voide.dev',
      apiBasePath: '/api',
    });

    expect(instance.config.apiBasePath).to.equal('/api');
  });

  it('Set URL on creation', () => {
    const instance = new DrupalSDK({
      url: 'https://drupal-sdk.prod.voide.dev',
    });

    expect(instance.config.url).to.equal('https://drupal-sdk.prod.voide.dev');
  });

  it('#login()', async () => {
    const fakeStorage = mockStorage({});
    const Drupal = new DrupalSDK({
      url: 'https://drupal-sdk.voide.dev',
      storage: fakeStorage,
    });

    sinon.stub(Drupal.auth, 'login');
    Drupal.login('username', 'password');

    expect(Drupal.auth.login).to.have.been.calledWith('username', 'password');
  });

  it('#logout()', async () => {
    const fakeStorage = mockStorage({});
    const Drupal = new DrupalSDK({
      url: 'https://drupal-sdk.voide.dev',
      storage: fakeStorage,
    });

    sinon.stub(Drupal.auth, 'logout');
    Drupal.logout();

    expect(Drupal.auth.logout).to.have.been.calledWith();
  });

  it('#get()', async () => {
    const fakeStorage = mockStorage({});
    const Drupal = new DrupalSDK({
      url: 'https://drupal-sdk.voide.dev',
      storage: fakeStorage,
    });

    const Node = Drupal.get('node');

    expect(Node).to.be.instanceOf(EntityStorage);
  });

  describe('path', () => {
    let Drupal: DrupalSDK;

    beforeEach(() => {
      const fakeStorage = mockStorage({});
      Drupal = new DrupalSDK({
        url: 'https://drupal-sdk.voide.dev',
        storage: fakeStorage,
      });
    });

    it('#getRoute()', async () => {
      sinon.stub(Drupal.api, 'get');

      Drupal.getRoute('/ondernemers');

      expect(Drupal.api.get).to.have.been.calledWith('router/translate-path', {
        path: '/ondernemers',
      });
    });

    it('#readByPath()', async () => {
      sinon.stub(Drupal, 'getRoute').resolves({
        jsonapi: {
          individual:
            'https://drupal-sdk.voide.dev/jsonapi/node/page/db8fdc95-b108-4fec-bdd3-ae2a20dbb5fa',
          resourceName: 'node--page',
          pathPrefix: 'jsonapi',
          basePath: '/jsonapi',
          entryPoint: 'https://drupal-sdk.voide.dev/jsonapi',
        },
      });
      sinon.stub(Drupal.api, 'get');
      await Drupal.readByPath('/lorem-ipsum');

      expect(Drupal.api.get).to.have.been.calledWith(
        'https://drupal-sdk.voide.dev/jsonapi/node/page/db8fdc95-b108-4fec-bdd3-ae2a20dbb5fa',
      );
    });

    it('#readByPath() Errors', async () => {
      sinon.stub(Drupal, 'getRoute').resolves({
        api: {
          individual:
            'https://drupal-sdk.voide.dev/jsonapi/node/page/db8fdc95-b108-4fec-bdd3-ae2a20dbb5fa',
          resourceName: 'node--page',
          pathPrefix: 'jsonapi',
          basePath: '/jsonapi',
          entryPoint: 'https://drupal-sdk.voide.dev/jsonapi',
        },
      });

      expect(Drupal.readByPath('/lorem-ipsum')).to.be.rejectedWith(Error);
    });
  });

  describe('Overridable readByPath method', () => {
    let Drupal: DrupalSDK;

    beforeEach(() => {
      const fakeStorage = mockStorage({});
      Drupal = new DrupalSDK({
        url: 'https://drupal-sdk.voide.dev',
        storage: fakeStorage,
        methods: {
          readByPath(
            path: string,
            inputParams: QueryParamsType,
            { api }: { api: APIInterface },
          ) {
            return api.get(`/route${path}`, inputParams);
          },
        },
      });
    });

    it('#readByPath() with override', async () => {
      sinon.stub(Drupal.api, 'get');
      await Drupal.readByPath('/lorem-ipsum');

      expect(Drupal.api.get).to.have.been.calledWith('/route/lorem-ipsum');
    });
  });

  describe('Overridable readByPath method', () => {
    let Drupal: DrupalSDK;

    beforeEach(() => {
      const fakeStorage = mockStorage({});
      Drupal = new DrupalSDK({
        url: 'https://drupal-sdk.voide.dev',
        storage: fakeStorage,
        methods: {
          getRoute(path: string, { api }: { api: APIInterface }) {
            return api.get('/route', { path });
          },
        },
      });
    });

    it('#getRoute() with overrides', async () => {
      sinon.stub(Drupal.api, 'get');

      Drupal.getRoute('/ondernemers');

      expect(Drupal.api.get).to.have.been.calledWith('/route', {
        path: '/ondernemers',
      });
    });
  });

  describe('RegisterMapping Method', () => {
    let Drupal: DrupalSDK;
    beforeEach(() => {
      const fakeStorage = mockStorage({});
      Drupal = new DrupalSDK({
        url: 'https://drupal-sdk.voide.dev',
        storage: fakeStorage,
      });
    });
    it('#registerMapping() with bundle', async () => {
      Drupal.registerMapping('node.script', {
        title: 'data.attributes.title',
        date: {
          start: 'data.attributes.field_date.value',
          end: 'data.attributes.field_date.end_value',
        },
      });

      expect(Drupal.entityMapping).to.deep.equal({
        node: {
          script: {
            title: 'data.attributes.title',
            date: {
              start: 'data.attributes.field_date.value',
              end: 'data.attributes.field_date.end_value',
            },
          },
        },
      });
    });

    it('#registerMapping() without bundle', async () => {
      Drupal.registerMapping('block', {
        title: 'data.attributes.title',
        date: {
          start: 'data.attributes.field_date.value',
          end: 'data.attributes.field_date.end_value',
        },
      });

      expect(Drupal.entityMapping).to.deep.equal({
        block: {
          block: {
            title: 'data.attributes.title',
            date: {
              start: 'data.attributes.field_date.value',
              end: 'data.attributes.field_date.end_value',
            },
          },
        },
      });
    });

    it('#registerMapping() without bundle', async () => {
      Drupal.registerMapping('node.script', {
        title: 'attributes.title',
        date: {
          start: 'attributes.field_date.value',
          end: 'attributes.field_date.end_value',
        },
      });
      Drupal.registerMapping('node.lorem', {
        title: 'attributes.title',
        date: {
          start: 'attributes.field_date.value',
          end: 'attributes.field_date.end_value',
        },
      });

      expect(Drupal.entityMapping).to.deep.equal({
        node: {
          script: {
            title: 'attributes.title',
            date: {
              start: 'attributes.field_date.value',
              end: 'attributes.field_date.end_value',
            },
          },
          lorem: {
            title: 'attributes.title',
            date: {
              start: 'attributes.field_date.value',
              end: 'attributes.field_date.end_value',
            },
          },
        },
      });
    });
  });

  describe('Date format method', () => {
    it('#formatApiDate() with invalid string', async () => {
      const date = '22-22-2022 20:22:01';
      expect(() => formatApiDate(date)).to.throw(
        `The given date (${date}) is invalid`,
      );
    });

    it('#formatApiDate() with string', async () => {
      const date = '2022-08-22T20:22:01';

      // Using the formatDate function, because the timezone can differ.
      const timezone = formatDate(date, 'O');
      expect(formatApiDate(date)).to.equal(`2022-08-22T20:22:01${timezone}`);
    });

    it('#formatApiDate() with date', async () => {
      const date = new Date('2022-08-22T20:22:01');

      // Using the formatDate function, because the timezone can differ.
      const timezone = formatDate(date, 'O');
      expect(formatApiDate(date)).to.equal(`2022-08-22T20:22:01${timezone}`);
    });

    it('#formatApiDate() with string', async () => {
      const date = '2022-08-22T20:22:01';

      // Using the formatDate function, because the timezone can differ.
      const timezone = formatDate(date, 'O');
      expect(formatApiDate(date)).to.equal(`2022-08-22T20:22:01${timezone}`);
    });

    it('#formatApiDate() with invalid string', async () => {
      const date = '22-22-2022 20:22:01';
      expect(() => formatApiDate(date)).to.throw(
        `The given date (${date}) is invalid`,
      );
    });

    it('#formatApiDate() with string', async () => {
      const date = '2022-08-22T20:22:01';
      expect(formatDate(date, 'dd-MM-yyyy HH:mm')).to.equal('22-08-2022 20:22');
    });

    it('#formatDate() with date', async () => {
      const date = new Date('2022-08-22T20:22:01');
      expect(formatDate(date, 'dd-MM-yyyy HH:mm')).to.equal('22-08-2022 20:22');
    });

    it('#formatDate() with string', async () => {
      const date = '2022-08-22T20:22:01';
      expect(formatDate(date, 'dd-MM-yyyy HH:mm')).to.equal('22-08-2022 20:22');
    });

    it('#formatDate() with locale', async () => {
      const date = '2022-08-22T20:22:01';
      expect(formatDate(date, 'dd MMMM yyyy', 'nl')).to.equal(
        '22 augustus 2022',
      );
    });
  });

  describe('User status methods', () => {
    it('#currentUser() with user', async () => {
      const fakeStorage = mockStorage({
        current_user: {
          uid: '1',
          roles: ['authenticated', 'administrator'],
          name: 'admin',
        },
      });
      const Drupal = new DrupalSDK({
        url: 'https://drupal-sdk.voide.dev',
        storage: fakeStorage,
        methods: {
          getRoute(path: string, { api }: { api: APIInterface }) {
            return api.get('/route', { path });
          },
        },
      });

      expect(Drupal.currentUser()).to.deep.equal({
        uid: '1',
        roles: ['authenticated', 'administrator'],
        name: 'admin',
      });
    });

    it('#currentUser() without user', async () => {
      const fakeStorage = mockStorage({});
      const Drupal = new DrupalSDK({
        url: 'https://drupal-sdk.voide.dev',
        storage: fakeStorage,
        methods: {
          getRoute(path: string, { api }: { api: APIInterface }) {
            return api.get('/route', { path });
          },
        },
      });

      expect(Drupal.currentUser()).to.equal(false);
    });
  });

  describe('json:api headers', () => {
    it('Check content-type header', async () => {
      const Drupal = new DrupalSDK({
        url: 'https://drupal-sdk.voide.dev/',
      });

      await Drupal.api.request('get', '/jsonapi/endpoint');

      expect(fetchStub).to.have.been.calledWith(
        'https://drupal-sdk.voide.dev/jsonapi/endpoint',
        {
          method: 'get',
          headers: {
            'Content-Type': 'application/vnd.api+json',
          },
          credentials: 'include',
        },
      );
    });
    it('Reformat api basepath and check content-type header', async () => {
      const Drupal = new DrupalSDK({
        url: 'https://drupal-sdk.voide.dev/',
        apiBasePath: '/api',
      });

      await Drupal.api.request('get', '/api/endpoint');

      expect(fetchStub).to.have.been.calledWith(
        'https://drupal-sdk.voide.dev/api/endpoint',
        {
          method: 'get',
          headers: {
            'Content-Type': 'application/vnd.api+json',
          },
          credentials: 'include',
        },
      );
    });
  });

  describe('Hooks', () => {
    it('Reformat base url by context', async () => {
      const Drupal = new DrupalSDK({
        url: 'https://drupal-sdk.voide.dev/',
        hooks: {
          beforeRequest: [
            async ({ requestOptions, context, ...args }) => {
              if (context.serverSide) {
                return {
                  ...args,
                  requestOptions: {
                    ...requestOptions,
                    baseURL: undefined,
                    url: `/api/drupal${requestOptions.url}`,
                  },
                };
              }
              return {
                ...args,
                requestOptions,
              };
            },
          ],
        },
      });

      await Drupal.withContext({ serverSide: true }).api.request(
        'get',
        '/endpoint',
      );

      expect(fetchStub).to.have.been.calledWith(
        'https://drupal-sdk.voide.dev/api/drupal/endpoint',
        {
          method: 'get',
          headers: {},
          credentials: 'include',
        },
      );
    });
  });

  it('#setStorage() method', async () => {
    const fakeStorage = mockStorage({
      current_user: {
        uid: '1',
        roles: ['authenticated', 'administrator'],
        name: 'admin',
      },
    });
    const Drupal = new DrupalSDK({
      url: 'https://drupal-sdk.voide.dev',
    });

    Drupal.setStorage(fakeStorage);

    expect(Drupal.currentUser()).to.deep.equal({
      uid: '1',
      roles: ['authenticated', 'administrator'],
      name: 'admin',
    });
  });
});
