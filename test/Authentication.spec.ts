/* eslint-disable max-nested-callbacks */
// tslint:disable
import * as chai from 'chai';
import * as sinonChai from 'sinon-chai';
import * as sinon from 'sinon';

import { mockStorage } from './mock/storage';
import { API, APIInterface } from '../src/API';
import { Configuration, ConfigurationInterface } from '../src/Configuration';
import { Authentication, AuthenticationInterface } from '../src/Authentication';
import { APIStorageInterface } from '../src/APIStorage';

const { expect } = chai;
chai.use(sinonChai);

describe('Authentication', () => {
  describe('Cookie Authentication', () => {
    let auth: AuthenticationInterface;
    let api: APIInterface;
    let config: ConfigurationInterface;

    describe('#login()', () => {
      beforeEach(() => {
        const fakeStorage = mockStorage({});
        config = new Configuration({
          url: 'https://drupal-sdk.voide.dev',
          authMode: 'cookie',
          storage: fakeStorage,
        });

        api = new API(config);

        auth = new Authentication(config, { api });

        const response = {
          csrf_token: 'random1token2string',
          logout_token: 'logout8token8string',
          current_user: {
            uid: 1,
            roles: ['authenticated', 'administrator'],
            name: 'admin',
          },
        };
        sinon.stub(api, 'post').resolves(response);
        sinon.stub(api, 'get').resolves(response);
        sinon.stub(api, 'put').resolves(response);
        sinon.stub(api, 'request').resolves(response);
      });

      afterEach(() => {
        sinon.restore();
      });

      it('Sets the url in use when passed in credentials', async () => {
        await auth.login('username', 'testPassword');

        expect(config.url).to.equal('https://drupal-sdk.voide.dev');
      });

      it('Calls request with the right parameters', async () => {
        await auth.login('username', 'testPassword');

        expect(api.post).to.have.been.calledWith(
          '/user/login',
          {
            name: 'username',
            pass: 'testPassword',
          },
          { _format: 'json' },
        );
      });

      it('Resolves with the currently logged in token, url, and project', async () => {
        const result = await auth.login('username', 'testPassword');
        expect(result).to.deep.equal({
          csrf_token: 'random1token2string',
          logout_token: 'logout8token8string',
          current_user: {
            uid: 1,
            roles: ['authenticated', 'administrator'],
            name: 'admin',
          },
        });
      });
    });

    describe('#logout()', () => {
      beforeEach(() => {
        const fakeStorage = mockStorage({
          csrf_token: 'random1token2string',
          logout_token: 'logout8token8string',
          current_user: {
            uid: 1,
            roles: ['authenticated', 'administrator'],
            name: 'admin',
          },
        });

        config = new Configuration({
          url: 'https://drupal-sdk.voide.dev',
          authMode: 'cookie',
          storage: fakeStorage,
        });

        api = new API(config);

        auth = new Authentication(config, { api });
        sinon.stub(api, 'post').resolves({});
        sinon.stub(api, 'get');
        sinon.stub(api, 'put');
        sinon.stub(api, 'request');
      });

      afterEach(() => {
        sinon.restore();
      });
      it('Calls the right API logout endpoint', async () => {
        await auth.logout();

        expect(api.post).to.have.been.calledWith(
          '/user/logout',
          {},
          {
            _format: 'json',
            token: 'logout8token8string',
          },
        );
      });
    });

    it('#requestPassword', async () => {
      const fakeStorage = mockStorage({});
      config = new Configuration({
        url: 'https://drupal-sdk.voide.dev',
        authMode: 'cookie',
        storage: fakeStorage,
      });

      api = new API(config);
      sinon.stub(api, 'post');
      auth = new Authentication(config, { api });

      await auth.requestPassword('admin@example.com');
      expect(api.post).to.have.been.calledWith(
        '/user/password',
        {
          mail: 'admin@example.com',
        },
        { _format: 'json' },
      );
    });

    it('#requestPasswordByUsername', async () => {
      const fakeStorage = mockStorage({});
      config = new Configuration({
        url: 'https://drupal-sdk.voide.dev',
        authMode: 'cookie',
        storage: fakeStorage,
      });

      api = new API(config);
      sinon.stub(api, 'post');

      auth = new Authentication(config, { api });

      await auth.requestPasswordByUsername('username');

      expect(api.post).to.have.been.calledWith(
        '/user/password',
        {
          name: 'username',
        },
        { _format: 'json' },
      );
    });

    describe('#loginByEmail()', () => {
      it('Calls Axios with the right parameters', async () => {
        expect(() =>
          auth.loginByEmail('admin@example.com', 'testPassword'),
        ).to.throw('No loginByEmail method was implemented.');
      });
    });
  });

  describe('Backend Authentication', () => {
    let auth: AuthenticationInterface;
    let api: APIInterface;
    let config: ConfigurationInterface;

    beforeEach(() => {
      const fakeStorage = mockStorage({});
      config = new Configuration({
        url: 'https://drupal-sdk.voide.dev',
        authMode: 'backend',
        storage: fakeStorage,
      });

      api = new API(config);

      auth = new Authentication(config, { api });

      sinon.stub(api, 'get').resolves('random1token2string');
      sinon.stub(api, 'post').resolves({
        config: {},
        data: {
          csrf_token: 'random1token2string',
          logout_token: 'logout8token8string',
          current_user: {
            uid: 1,
            roles: ['authenticated', 'administrator'],
            name: 'admin',
          },
        },
        headers: {},
        status: 200,
        statusText: 'OK',
      });
    });

    describe('#login()', () => {
      it('Calls Axios with the right parameters', async () => {
        await auth.login('username', 'testPassword');

        expect(api.get).to.have.been.calledWith('/session/token', {
          _format: 'json',
        });
      });

      it('Resolves with the currently logged in token, url, and project', async () => {
        const result = await auth.login('username', 'testPassword');

        expect(result).to.deep.equal('random1token2string');
      });
    });
  });

  describe('Authentication with overrides', () => {
    let auth: AuthenticationInterface;
    let api: APIInterface;
    let config: ConfigurationInterface;
    const fakeStorage = mockStorage({});

    beforeEach(() => {
      config = new Configuration({
        url: 'https://drupal-sdk.voide.dev',
        authMode: 'cookie',
        storage: fakeStorage,
        methods: {
          loginByEmail(
            mail: string,
            password: string,
            {
              api: internalAPI,
              storage,
            }: { api: APIInterface; storage: APIStorageInterface },
          ) {
            const body: {} = {
              mail,
              pass: password,
            };

            return internalAPI
              .post('/user/email-login', body, { _format: 'json' })
              .then((json: any) => {
                internalAPI.setCSRF(json.csrf_token);
                storage.setItem('logout_token', json.logout_token);
                return json;
              });
          },
        },
      });

      api = new API(config);

      auth = new Authentication(config, { api });

      sinon.stub(api, 'post').resolves({
        csrf_token: 'random1token2string',
        logout_token: 'logout8token8string',
      });
    });

    describe('#loginByEmail()', () => {
      it('Calls Axios with the right parameters', async () => {
        await auth.loginByEmail('admin@example.com', 'testPassword');

        expect(api.post).to.have.been.calledWith(
          '/user/email-login',
          { mail: 'admin@example.com', pass: 'testPassword' },
          { _format: 'json' },
        );
      });
    });
  });
});
