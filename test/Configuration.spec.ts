/* eslint-disable max-nested-callbacks */
import * as chai from 'chai';
import * as sinonChai from 'sinon-chai';
import DrupalSDK from '../src/index';

const { expect } = chai;
chai.use(sinonChai);

describe('Configuration Class', () => {
  describe('initial', () => {
    it('url', () => {
      const instance = new DrupalSDK({
        url: 'https://drupal-sdk.prod.voide.dev',
      });

      expect(instance.config.url).to.equal('https://drupal-sdk.prod.voide.dev');
    });

    it('token', () => {
      const instance = new DrupalSDK({
        url: 'https://drupal-sdk.prod.voide.dev',
        token: 'expectedToken',
      });

      expect(instance.config.token).to.equal('expectedToken');
    });

    it('authMode', () => {
      const instance = new DrupalSDK({
        url: 'https://drupal-sdk.prod.voide.dev',
        authMode: 'jwt',
      });

      expect(instance.config.authMode).to.equal('jwt');
    });

    it('storageKey', () => {
      const instance = new DrupalSDK({
        url: 'https://drupal-sdk.prod.voide.dev',
        storageKey: 'custom-storage-key',
      });

      expect(instance.config.storageKey).to.equal('custom-storage-key');
    });

    it('apiBasePath', () => {
      const instance = new DrupalSDK({
        url: 'https://drupal-sdk.prod.voide.dev',
        apiBasePath: '/api',
      });

      expect(instance.config.apiBasePath).to.equal('/api');
    });

    it('tokenExpirationTime', () => {
      const instance = new DrupalSDK({
        url: 'https://drupal-sdk.prod.voide.dev',
        tokenExpirationTime: 1234,
      });

      expect(instance.config.tokenExpirationTime).to.equal(1234);
    });
  });

  describe('defaults', () => {
    it('authMode', () => {
      const instance = new DrupalSDK({
        url: 'https://drupal-sdk.test.voide.dev',
      });

      expect(instance.config.authMode).to.equal('cookie');
    });

    it('apiBasePath', () => {
      const instance = new DrupalSDK({
        url: 'https://drupal-sdk.test.voide.dev',
      });

      expect(instance.config.apiBasePath).to.equal('/jsonapi');
    });

    it('storageKey', () => {
      const instance = new DrupalSDK({
        url: 'https://drupal-sdk.test.voide.dev',
      });

      expect(instance.config.storageKey).to.equal('drupal-sdk');
    });

    it('useDecoupledRouter', () => {
      const instance = new DrupalSDK({
        url: 'https://drupal-sdk.test.voide.dev',
      });

      expect(instance.config.useDecoupledRouter).to.equal(true);
    });

    it('tokenExpirationTime', () => {
      const instance = new DrupalSDK({
        url: 'https://drupal-sdk.prod.voide.dev',
      });

      expect(instance.config.tokenExpirationTime).to.equal(5 * 6 * 1000);
    });
  });

  describe('override', () => {
    it('url', () => {
      const instance = new DrupalSDK({
        url: 'https://drupal-sdk.test.voide.dev',
      });

      instance.config.url = 'https://drupal-sdk.prod.voide.dev';
      expect(instance.config.url).to.equal('https://drupal-sdk.prod.voide.dev');
    });

    it('token', () => {
      const instance = new DrupalSDK({
        url: 'https://drupal-sdk.prod.voide.dev',
        token: 'initialToken',
      });

      instance.config.token = 'expectedToken';
      expect(instance.config.token).to.equal('expectedToken');
    });

    it('authMode', () => {
      const instance = new DrupalSDK({
        url: 'https://drupal-sdk.prod.voide.dev',
        authMode: 'cookie',
      });

      instance.config.authMode = 'jwt';
      expect(instance.config.authMode).to.equal('jwt');
    });

    it('storageKey', () => {
      const instance = new DrupalSDK({
        url: 'https://drupal-sdk.prod.voide.dev',
        storageKey: 'drupal-sdk',
      });

      instance.config.storageKey = 'custom-storage-key';
      expect(instance.config.storageKey).to.equal('custom-storage-key');
    });

    it('apiBasePath', () => {
      const instance = new DrupalSDK({
        url: 'https://drupal-sdk.prod.voide.dev',
        apiBasePath: '/api',
      });

      instance.config.apiBasePath = '/api';
      expect(instance.config.apiBasePath).to.equal('/api');
    });

    it('tokenExpirationTime', () => {
      const instance = new DrupalSDK({
        url: 'https://drupal-sdk.prod.voide.dev',
      });

      instance.config.tokenExpirationTime = 10;
      expect(instance.config.tokenExpirationTime).to.equal(10);
    });

    it('ignoreSsl', () => {
      const instance = new DrupalSDK({
        url: 'https://drupal-sdk.prod.voide.dev',
      });

      instance.config.ignoreSsl = true;
      expect(instance.config.ignoreSsl).to.equal(true);
    });

    it('debug', () => {
      const instance = new DrupalSDK({
        url: 'https://drupal-sdk.prod.voide.dev',
      });

      instance.config.debug = true;
      expect(instance.config.debug).to.equal(true);
    });

    it('useDecoupledRouter', () => {
      const instance = new DrupalSDK({
        url: 'https://drupal-sdk.prod.voide.dev',
      });

      instance.config.useDecoupledRouter = false;
      expect(instance.config.useDecoupledRouter).to.equal(false);
    });
  });
});
