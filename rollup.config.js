export default {
  onwarn(warning) {
    // Skip certain warnings

    // should intercept ... but doesn't in some rollup versions
    if (warning.code === 'THIS_IS_UNDEFINED') {
      return;
    }

    // eslint-disable-next-line no-console
    console.warn(warning.message, warning.code);
  },
};
