# Drupal SDK

## Introduction

The Drupal SDK is a helper package for calling Drupal endpoints, like the JSON:API, in a more efficient and easy way.

## Installation

```
npm install drupal-sdk
```


## Development & Bugtracking
Development and bugtracking take place on gitlab.com. See https://gitlab.com/VoidE/drupal-sdk/-/issues for the current issue queue and https://gitlab.com/VoidE/drupal-sdk for the current repository.



## Usage

```JS
const Drupal = new DrupalSDK({
  url: 'https://drupal-sdk.prod.voide.dev',
});

async fetchData() {
  const items = await Drupal.get('node', 'article').read('12f930ee-8a7b-11eb-8dcd-0242ac130003');

  await Drupal.login('username', password);

  Drupal.get('node').create({
    title: 'Lorem ipsum',
  });
}
```


## Configuration

| Name                | Description                                                  | Type    | Required | Default    |
| ------------------- | ------------------------------------------------------------ | ------- | -------- | ---------- |
| url                 | The URL of the Drupal environment                            | string  | true     |            |
| authMode            | The authentication mode to use. For now, only "cookie" is available, but "jwt" will be implemented later on. | string  | false    | cookie     |
| storage             | The storage object for storing CSRF tokens and jwt tokens. This object must contain a "setItem" and "getItem" method. Only required when using authentication. | object  | false    |            |
| storageKey          | The key to use in your storage object.                       | string  | false    | drupal-sdk |
| apiBasePath         | The api base path to use in the JSON:API related methods     | string  | false    | /jsonapi   |
| useDecoupledRouter  | Wheter to use a decoupled router method or not. If true, make sure the https://www.drupal.org/project/decoupled_router module is installed in your drupal website | boolean | false    | false      |
| token               | The authentication token for usage without login             | string  | false    |            |
| tokenExpirationTime | The expiration time of the login token                       | number  | false    | 30000      |
| methods             | An object with methods to override. See the list below.      | object  | false    | {}         |


## Overridable Methods

For the following methods, overrides are supported.

- getRoute
- readByPath
- loginByEmail

All methods will come with an `injectableProps` argument. This is an object of helper classes in the DrupalSDK. Available classes are:

- `config` - The configuration object passed to the main class.
- `api` - The API class to perform API calls to.
- `auth` - The Authentication helper
- `storage` - The storage helper to store data in.


### getRoute()

**Arguments**

- path
- injectableProps

**Example**

```JS
const Drupal = new DrupalSDK({
  url: 'https://drupal-sdk.voide.dev',
  methods: {
    getRoute(path, { api }) {
      return api.get('/route', { path });
    },
  },
});
```

### readbyPath()

**Arguments**

- path
- injectableProps

**Example**

```JS
const Drupal = new DrupalSDK({
  url: 'https://drupal-sdk.voide.dev',
  methods: {
    readByPath(path, inputParams, { api }) {
      return Drupal.getRoute(path).then((json) => api.get(json.path));
    },
  },
});
```


### loginByEmail()

**Arguments**

- mail
- Password
- injectableProps

**Example**

```JS
const Drupal = new DrupalSDK({
  url: 'https://drupal-sdk.voide.dev',
  methods: {
    readByPath(path, inputParams, { api, storage }) {
      const body = {
        mail,
        pass: password,
      };

      return api
        .post('/user/email-login', body, { _format: 'json' })
        .then((json) => {
          api.setCSRF(json.csrf_token);
          storage.setItem('logout_token', json.logout_token);
          return json;
        });
    },
  },
});
```

## Entities and Storages

In the Drupal SDK a proxy layer between the SDK object and the entity is used. In the context of the SDK and Drupal, we call this layer the EntityStorage.

To get the storage of an entity, please use the following code:

```JS
const nodeStorage = Drupal.get('node');
```

The argument of this `get`-method is the machine name of the entity.

### `read` a Single Entity

**Arguments**

 - uuid
 - options (optional)

```JS
await Drupal.get('node').read('12f930ee-8a7b-11eb-8dcd-0242ac130003');
```

### `create` a Single Entity -

**Arguments**

 - body
 - options (optional)

```JS
Drupal.get('node', 'article').create({ attributes: { title: 'Lorem' } }, { filter: { lorem: 'ipsum' } });
```

### `update` a Single Entity

**Arguments**

 - uuid
 - body
 - options (optional)

```JS
Drupal.get('node', 'article').update('74518cfa-77ad-11eb-9439-0242ac130002', { attributes: { title: 'Lorem' } });
```

### `delete` a Single Entity

**Arguments**

 - uuid
 - options (optional)

```JS
Drupal.get('node', 'article').delete('74518cfa-77ad-11eb-9439-0242ac130002');
```

## Authentication

Accessing protected endpoints will be easy using the Drupal SDK. Just login or provide an access token to the configuration object.

For now, only cookie authentication is available, so let's take a look into that.



### login()

**Arguments**

 - username
 - Password

```JS
Drupal.login('username', 'password');
```



### logout()

```JS
Drupal.logout();
```



### loginByEmail()

This method is not available by default, but can be created in the `methods` section of the main configuration object.

**Arguments**

 - username
 - password

```JS
Drupal.loginByEmail('admin@example.com', 'password');
```



### requestPassword()

**Arguments**

 - mail

```JS
Drupal.requestPassword('admin@example.com');
```



### requestPasswordByUsername()

**Arguments**

 - username

```JS
Drupal.requestPassword('username');
```
