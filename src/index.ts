/**
 * @module exports
 */

import { DateTime } from 'luxon';
import { Connection } from './Connection';

/**
 * Format input date to a custom format.
 *
 * @param {string|Date} inputDate
 *   The date to process.
 * @param {string} dateFormat
 *   The format to use.
 * @param {string|undefined} locale
 *   The country code for locale options.
 *
 * @see https://moment.github.io/luxon/#/formatting?id=table-of-tokens
 *
 * @return {string}
 *   The date string.
 */
export const formatDate = (
  inputDate: string | Date,
  dateFormat: string,
  locale?: string,
) => {
  let date;
  if (inputDate instanceof Date) {
    date = inputDate;
  } else {
    date = new Date(inputDate);
  }

  if (!Number.isNaN(date.getTime())) {
    let dt = DateTime.fromJSDate(date);
    if (locale) {
      dt = dt.setLocale(locale);
    }
    return dt.toFormat(dateFormat);
  }

  throw new Error(`The given date (${inputDate}) is invalid`);
};

/**
 * Format input date to Drupal JSON:API acceptable format
 *
 * @param {string|Date} inputDate
 *   The date to process.
 * @param {string|undefined} locale
 *   The country code for locale options.
 *
 * @return {string}
 *   The date string.
 */
export const formatApiDate = (inputDate: string | Date, locale?: string) => {
  return formatDate(inputDate, "yyyy-MM-dd'T'HH:mm:ssO", locale);
};

export * from './schemes/hooks';
export * from './Error';
export default Connection;
