export interface APIStorageDriverInterface {
  getItem<T extends any = any>(key: string): T;
  setItem(key: string, value: any): void;
  removeItem(key: string): void;
}

export interface APIStorageInterface {
  storageKey: string;
  getItem<T extends any = any>(key: string): T;
  setItem(key: string, value: any): void;
  removeItem(key: string): void;
}

export class APIStorage implements APIStorageInterface {
  public storageDriver: APIStorageDriverInterface;

  public storageKey: string;

  /**
   * Constructor of the APIStorage class.
   *
   * @param {string} storageDriver
   *   The storage driver.
   * @param {string} storageKey
   *   The storage key.
   */
  constructor(storageDriver: APIStorageDriverInterface, storageKey: string) {
    this.storageDriver = storageDriver;
    this.storageKey = storageKey;
  }

  /**
   * Get an item from the storage.
   *
   * @param {string} key
   *   The key of the item.
   *
   * @return {any}
   *   The stored data.
   */
  public getItem(key: string) {
    if (this.storageDriver !== undefined) {
      const storedData = this.storageDriver.getItem(this.storageKey);
      const parsedData = JSON.parse(storedData) || {};

      return parsedData[key] || undefined;
    }

    return undefined;
  }

  /**
   * Get an item from the storage.
   *
   * @param {string} key
   *   The key of the item.
   * @param {any} value
   *   The value of the item.
   *
   * @return {any}
   *   The stored item.
   */
  public setItem(key: string, value: any) {
    if (this.storageDriver !== undefined) {
      const storedData = this.storageDriver.getItem(this.storageKey);
      const parsedData = JSON.parse(storedData) || {};

      const newData = { ...parsedData, [key]: value };

      this.storageDriver.setItem(this.storageKey, JSON.stringify(newData));
      return parsedData[key];
    }
    return undefined;
  }

  /**
   * Delete an item from the storage.
   *
   * @param {string} key
   *   The key of the item.
   */
  public removeItem(key: string) {
    if (this.storageDriver !== undefined) {
      const storedData = this.storageDriver.getItem(this.storageKey);
      const parsedData = JSON.parse(storedData);
      delete parsedData[key];

      this.storageDriver.setItem(this.storageKey, JSON.stringify(parsedData));
    }
  }
}
