/**
 * @module API
 */

import axios, { AxiosInstance } from 'axios';
import * as https from 'https';
import { forEachSeries } from 'p-iteration';
import { querify } from './utils/querify';
import { ConfigurationInterface } from './Configuration';
import {
  AvailableHooks,
  ContextType,
  HookNames,
  BeforeRequestHook,
  AfterRequestHook,
} from './schemes/hooks';
import { isString } from './utils/checkType';
import { APIError, ErrorViolationType } from './Error';
import { APIStorage, APIStorageInterface } from './APIStorage';

type BaseRquestMethds = 'get' | 'post' | 'put' | 'patch' | 'delete';
export type RequestMethod =
  | Lowercase<BaseRquestMethds>
  | Uppercase<BaseRquestMethds>;

export type RequestOptions = { [key: string]: any };

export interface APIInterface {
  get<T extends any = any>(
    endpoint: string,
    params?: object,
    options?: RequestOptions,
  ): Promise<T>;
  post<T extends any = any>(
    endpoint: string,
    body: object,
    params?: object,
    options?: RequestOptions,
  ): Promise<T>;
  patch<T extends any = any>(
    endpoint: string,
    body: object,
    params?: object,
    options?: RequestOptions,
  ): Promise<T>;
  put<T extends any = any>(
    endpoint: string,
    body: object,
    params?: object,
    options?: RequestOptions,
  ): Promise<T>;
  delete<T extends any = any>(
    endpoint: string,
    params?: object,
    options?: RequestOptions,
  ): Promise<T>;
  request<T extends any = any>(
    method: RequestMethod,
    endpoint: string,
    params?: object,
    data?: any,
    headers?: { [key: string]: string },
    options?: RequestOptions,
  ): Promise<T>;
  headers: { [key: string]: string };
  setContext(args: ContextType): APIInterface;
  setHeader(name: string, value: string): void;
  setCSRF(token: string): string;
  getCSRF(): string;
  removeCSRF(): void;
  setRequestCookie(cookie: string): string;
  getRequestCookie(): string;
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  executeHook<T extends AvailableHooks>(
    hookName: HookNames,
    injectableProps: any,
  ): Promise<any>;
}

export class API implements APIInterface {
  public config: ConfigurationInterface;

  public storage: APIStorageInterface;

  private csrfToken: string;

  private requestCookie: string;

  private requestContext?: { [key: string]: any };

  public headers: { [key: string]: string } = {};

  /**
   * Constructor of the API class.
   *
   * @param {ConfigurationInterface} config
   *   The configuration to use.
   */
  constructor(config: ConfigurationInterface) {
    this.config = config;
    const { storageKey } = this.config;
    this.storage = new APIStorage(this.config.storage, storageKey);

    const axiosOptions = {
      paramsSerializer: querify,
      timeout: 10 * 60 * 1000, // 10 min
      withCredentials: true,
    };

    if (config.authMode === 'cookie') {
      axiosOptions.withCredentials = true;
    }

    this.requestCookie = '';
    this.csrfToken = '';
  }

  /**
   * Set custom header.
   *
   * @param {object} args
   *   The args for the method.
   * @param {contextMethod} method
   *   The method itself.
   *
   * @return {APIInterface}
   *   Returns the object itself.
   */
  setContext = (args: ContextType) => {
    this.requestContext = args;

    return this;
  };

  /**
   * Set custom header.
   *
   * @param {string} name
   *   The name of the header to set.
   * @param {string} value
   *   The value of the header to set.
   */
  setHeader = (name: string, value: string) => {
    this.headers = { ...this.headers, [name]: value };
  };

  /**
   * Sets the CSRF Token.
   *
   * @param {string} token
   *   The CSRF token to set.
   *
   * @return {string}
   *   Returns the given token.
   */
  setCSRF(token: string) {
    this.csrfToken = token;
    this.storage.setItem('csrf_token', token);

    return token;
  }

  /**
   * Get the CSRF Token.
   *
   * @return {string}
   *   The CSRF Token.
   */
  getCSRF() {
    return this.storage.getItem('csrf_token') || this.csrfToken;
  }

  /**
   * Get the CSRF Token.
   */
  removeCSRF() {
    this.csrfToken = '';
    this.storage.removeItem('csrf_token');
  }

  /**
   * Sets the Request Cookie.
   *
   * @param {string} cookie
   *   The Request Cookie to set.
   *
   * @return {string}
   *   Returns the given request cookie.
   */
  setRequestCookie(cookie: string) {
    this.requestCookie = cookie;
    this.storage.setItem('request_cookie', cookie);
    return cookie;
  }

  /**
   * Execute a hook.
   *
   * @param {string} hookName
   *   The name of the hook to execute.
   * @param {any} injectableProps
   *   The props to process.
   *
   * @return {any}
   *   The processed props.
   */
  async executeHook<T extends AvailableHooks>(
    hookName: HookNames,
    injectableProps: any,
  ) {
    let args = injectableProps;
    if (this.config.hooks && this.config.hooks[hookName]) {
      const hooks = this.config.hooks[hookName] as T[];
      await forEachSeries(hooks, async (hook: T) => {
        args = await hook({ ...args, context: this.requestContext || {} });
      });
    }

    return args;
  }

  /**
   * Get the Request Cookie.
   *
   * @return {string}
   *   The Request Cookie.
   */
  getRequestCookie() {
    return this.storage.getItem('request_cookie') || this.requestCookie;
  }

  /**
   * Perform GET request.
   *
   * @param {string} endpoint
   *   The endpoint to send the request to.
   * @param {object} params
   *   The extra query parameters to add in the request.
   * @param {object} options
   *   The extra options to add the request.
   *
   * @return {Promise}
   *   Return a promise of the Axios Request.
   */
  get(endpoint: string, params = {}, options?: RequestOptions) {
    return this.request('get', endpoint, params, {}, {}, options);
  }

  /**
   * Perform POST request.
   *
   * @param {string} endpoint
   *   The endpoint to send the request to.
   * @param {object} body
   *   The request body.
   * @param {object} params
   *   The extra query parameters to add in the request.
   * @param {object} options
   *   The extra options to add the request.
   *
   * @return {Promise}
   *   Return a promise of the Axios Request.
   */
  post(endpoint: string, body: {}, params = {}, options?: RequestOptions) {
    return this.request('post', endpoint, params, body, {}, options);
  }

  /**
   * Perform PATCH request.
   *
   * @param {string} endpoint
   *   The endpoint to send the request to.
   * @param {object} body
   *   The request body.
   * @param {object} params
   *   The extra query parameters to add in the request.
   * @param {object} options
   *   The extra options to add the request.
   *
   * @return {Promise}
   *   Return a promise of the Axios Request.
   */
  patch(endpoint: string, body: {}, params = {}, options?: RequestOptions) {
    return this.request('patch', endpoint, params, body, {}, options);
  }

  /**
   * Perform PUT request.
   *
   * @param {string} endpoint
   *   The endpoint to send the request to.
   * @param {object} body
   *   The request body.
   * @param {object} params
   *   The extra query parameters to add in the request.
   * @param {object} options
   *   The extra options to add the request.
   *
   * @return {Promise}
   *   Return a promise of the Axios Request.
   */
  put(endpoint: string, body: {}, params = {}, options?: RequestOptions) {
    return this.request('put', endpoint, params, body, {}, options);
  }

  /**
   * Perform DELETE request.
   *
   * @param {string} endpoint
   *   The endpoint to send the request to.
   * @param {object} params
   *   The extra query parameters to add in the request.
   * @param {object} options
   *   The extra options to add the request.
   *
   * @return {Promise}
   *   Return a promise of the Axios Request.
   */
  delete(endpoint: string, params = {}, options?: RequestOptions) {
    return this.request('delete', endpoint, params, {}, options);
  }

  /**
   * Makes an HTTP request.
   *
   * @param {RequestMethod} method
   *   The HTTP method to use.
   * @param {string} endpoint
   *   The endpoint to request.
   * @param {object} [params={}]
   *   The query parameters.
   * @param {any} [data={}]
   *   The body of the request.
   * @param {object} [headers={}]
   *   The headers of the request.
   * @param {object} [options={}]
   *   The options of the request.
   * @return {Promise<any>}
   *  A promise that resolves with the response data.
   */
  async request(
    method: RequestMethod,
    endpoint: string,
    params: { [key: string]: any } = {},
    data: any = {},
    headers: { [key: string]: string } = {},
    options: RequestOptions = {},
  ) {
    let api: APIInterface = this;
    if (!this.config.url) {
      throw new Error('Drupal SDK has no URL configured to send requests to.');
    }
    let baseURL = `${this.config.url}`;
    if (baseURL.endsWith('/') === false) baseURL += '/';
    const headerObject: { [key: string]: any } = {};
    const requestOptions: { [key: string]: any } = {
      ...options,
      method,
      credentials: 'include',
      url: endpoint,
      headers: {
        ...headerObject,
        ...api.headers,
        ...options.headers,
        ...headers,
      },
    };

    method = method.toLowerCase() as BaseRquestMethds;
    if (method !== 'get') {
      requestOptions.body = JSON.stringify(data);
    }
    if (
      this.config.token &&
      isString(this.config.token) &&
      this.config.token.length > 0
    ) {
      headerObject.Authorization = `Bearer ${this.config.token}`;
    }
    const hasCSRF =
      this.config.authMode === 'cookie' || this.config.authMode === 'backend';
    if (hasCSRF) {
      if (this.getCSRF()) {
        headerObject['X-CSRF-TOKEN'] = this.getCSRF();
      }
      if (this.getRequestCookie()) {
        headerObject.Cookie = this.getRequestCookie();
      }
    }
    if (this.config.basicAuth !== undefined) {
      headerObject.Authorization = `Basic ${btoa(
        `${this.config.basicAuth.username}:${this.config.basicAuth.password}`,
      )}`;
    }
    const slashEndpoint = endpoint.startsWith('/') ? endpoint : `/${endpoint}`;
    const isJsonAPPI = slashEndpoint.includes(
      this.config.apiBasePath || '/jsonapi',
    );
    if (isJsonAPPI) {
      headerObject['Content-Type'] = 'application/vnd.api+json';
    }

    requestOptions.headers = { ...headerObject, ...requestOptions.headers };
    const hookResult = await this.executeHook<BeforeRequestHook>(
      'beforeRequest',
      {
        api,
        requestOptions,
      },
    );
    api = hookResult.api;
    const { url: finalUrl, ...finalRequestOptions } = hookResult.requestOptions;
    const url = new URL(finalUrl || endpoint, baseURL);
    Object.keys(params).forEach((key) =>
      url.searchParams.append(key, params[key]),
    );

    delete finalRequestOptions.baseURL;

    const result = fetch(url.toString(), finalRequestOptions)
      .then((res) => {
        if (Number(res.status) >= 400) throw res;
        return res;
      })
      .then((res) => {
        if (res.headers.has('set-cookie')) {
          const cookie = res.headers.get('set-cookie');
          if (cookie) {
            api.setRequestCookie(cookie);
          }
        }
        return res;
      })
      .then((res) => res.json())
      .then(async (responseData) => {
        const afterRequestHook = await this.executeHook<AfterRequestHook>(
          'afterRequest',
          {
            responseData,
            finalRequestOptions,
          },
        );

        return afterRequestHook.data || responseData;
      });

    if (requestOptions.returnRaw) {
      return result;
    }

    return result.catch(async (error) => {
      if (!error.json) throw error;

      const baseErrorInfo = {
        error,
        url: requestOptions.url,
        responseUrl: '',
        method: requestOptions.method,
        params: requestOptions.params,
        message: '',
        data: '',
        code: error.status || -1,
      };

      const errorResponseData = await error.json();
      baseErrorInfo.message = errorResponseData.message || '';
      baseErrorInfo.data = errorResponseData || '';

      if (error.request && error.request.res && error.request.res.responseUrl) {
        baseErrorInfo.responseUrl = error.request.res.responseUrl;
      }

      if (errorResponseData && errorResponseData.message) {
        throw new APIError(
          errorResponseData.message || 'Unknown error occured',
          baseErrorInfo,
        );
      }

      if (
        errorResponseData &&
        errorResponseData.jsonapi &&
        Number(error.status) === 422
      ) {
        const violations: ErrorViolationType[] = [];
        errorResponseData.errors.forEach((violationError: any) => {
          const violation: ErrorViolationType = {
            detail: violationError.detail,
          };

          if (violationError.detail.includes(':')) {
            violation.message = violationError.detail
              .substring(violationError.detail.indexOf(':') + 1)
              .trim();
            violation.fieldName = violationError.detail
              .substring(0, violationError.detail.indexOf(':'))
              .trim();
          }

          violations.push(violation);
        });

        throw new APIError('Unprocessable Content', {
          ...baseErrorInfo,
          violations,
          code: 422,
        });
      }

      throw new APIError('Unknown error occured', baseErrorInfo);
    });
  }
}
