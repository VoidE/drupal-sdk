import { AxiosError, AxiosRequestConfig } from 'axios';

export interface ErrorResponseDataInterface {
  error?: {
    code: string;
    message: string;
  };
}

export type ErrorViolationType = {
  detail: string;
  fieldName?: string;
  message?: string;
};

export interface ErrorResponseMetaInterface {
  json?: boolean;
  error: Error;
  data: ErrorResponseDataInterface;
  status: number;
  statusText: string;
  headers: Record<string, string>;
  config: AxiosRequestConfig;
}

export interface ErrorResponseInterface extends AxiosError {
  request?: object;
  response?: ErrorResponseMetaInterface;
}
export class APIError extends Error {
  /**
   * @param {string} message
   *   The error message string.
   * @param {object} info
   *   The info of the error.
   */
  constructor(
    public message: string,
    private info: {
      code: number | string;
      method: string;
      url: string;
      responseUrl?: string;
      params?: object;
      error?: ErrorResponseInterface;
      data?: any;
      violations?: ErrorViolationType[];
    },
  ) {
    super(message); // 'Error' breaks prototype chain here
    Object.setPrototypeOf(this, new.target.prototype); // restore prototype chain
  }

  /**
   * Get the url.
   */
  public get url() {
    return this.info.url;
  }

  /**
   * Get the method.
   */
  public get method() {
    return this.info.method.toUpperCase();
  }

  /**
   * Get the responseUrl.
   */
  public get responseUrl() {
    return this.info.responseUrl || '';
  }

  /**
   * Get the code.
   */
  public get code() {
    return `${this.info.code}`;
  }

  /**
   * Get the params.
   */
  public get params() {
    return this.info.params || {};
  }

  /**
   * Get the full data.
   */
  public get data() {
    return this.info.data || {};
  }

  /**
   * Get the violations.
   */
  public get violations() {
    return this.info.violations || [];
  }

  /**
   * Convert to string.
   *
   * @return {string}
   *   Returns the error string.
   */
  public toString() {
    let output = [
      'Drupal call failed:',
      `${this.method} ${this.url} ${JSON.stringify(this.params)} -`,
      this.message,
      `(code ${this.code})`,
    ].join(' ');

    if (this.responseUrl !== '') {
      output = `${output} - Original URL: ${this.responseUrl}`;
    }

    return output;
  }
}
