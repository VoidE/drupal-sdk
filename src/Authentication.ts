/**
 * @module Authentication
 */

import { UserInterface } from './schemes/drupal/User';
import { APIStorage, APIStorageInterface } from './APIStorage';
import { ConfigurationInterface } from './Configuration';
import { APIInterface } from './API';
import { CookieAuthentication } from './auth/CookieAuthentication';
import { AuthenticationProviderInterface } from './auth/BaseAuthentication';
import { BackendAuthentication } from './auth/BackendAuthentication';

export type AuthModes = 'jwt' | 'cookie' | 'backend';

export interface LoginOptionsInterface {
  persist: boolean;
  storage: boolean;
  authMode: AuthModes;
  url?: string | undefined;
}

/* eslint-disable camelcase */
export interface AuthenticationResponseInterface {
  current_user: UserInterface;
  csrf_token: string;
  logout_token: string;
}

export interface LoginBodyInterface {
  name: string;
  pass: string;
}

export interface AuthenticationInterface {
  api: APIInterface;
  config: ConfigurationInterface;
  login(
    username: string,
    password: string,
    options?: LoginOptionsInterface | undefined,
  ): Promise<AuthenticationResponseInterface>;
  logout(): Promise<any>;
  requestPassword(email: string): Promise<any>;
  requestPasswordByUsername(username: string): Promise<any>;
  loginByEmail(mail: string, password: string): Promise<any>;
}

interface AuthenticationInjectInterface {
  api: APIInterface;
}

export class Authentication {
  public api: APIInterface;

  public config: ConfigurationInterface;

  public authDriver: AuthenticationProviderInterface;

  public storage?: APIStorageInterface;

  public injectableProps: {};

  /**
   * Creates a new authentication instance.
   *
   * @param {ConfigurationInterface} config
   *   The configuration object.
   * @param {AuthenticationInjectInterface} inject
   *   Defined classes to inject.
   */
  constructor(
    config: ConfigurationInterface,
    { api }: AuthenticationInjectInterface,
  ) {
    this.api = api;
    this.config = config;

    const { storageKey } = this.config;
    this.storage = new APIStorage(this.config.storage, storageKey);

    // @todo Implement jwt method.
    switch (this.config.authMode) {
      case 'backend':
        this.authDriver = new BackendAuthentication(api, this.storage);
        break;
      default:
        this.authDriver = new CookieAuthentication(api, this.storage);
    }

    this.injectableProps = {
      api: this.api,
      config: this.config,
      storage: this.storage,
      authDriver: this.authDriver,
    };
  }

  /**
   * Perform login request.
   *
   * @param  {string} username
   *   The username of the user.
   * @param  {string} password
   *   The password of the user.
   *
   * @return {Promise}
   *   The promise of the api request.
   */
  public login(
    username: string,
    password: string,
  ): Promise<AuthenticationResponseInterface> {
    return this.authDriver.login(username, password);
  }

  /**
   * Perform login request by email.
   *
   * @param  {string} mail
   *   The email address of the user.
   * @param  {string} password
   *   The password of the user.
   *
   * @return {Promise}
   *   The promise of the api request.
   */
  public loginByEmail(mail: string, password: string) {
    if (this.config.methods.loginByEmail !== undefined) {
      return this.config.methods.loginByEmail(
        mail,
        password,
        this.injectableProps,
      );
    }

    throw Error('No loginByEmail method was implemented.');
  }

  /**
   * Perform login request.
   *
   * @return {Promise}
   *   The promise of the api request.
   */
  public logout(): Promise<any> {
    return this.authDriver.logout();
  }

  /**
   * Perform password reset request.
   *
   * @param {string} email
   *   The email of the user.
   *
   * @return {Promise}
   *   The promise of the api request.
   */
  public requestPassword(email: string): Promise<any> {
    return this.authDriver.requestPassword(email);
  }

  /**
   * Perform password reset request by username.
   *
   * @param {string} username
   *   The username of the user.
   *
   * @return {Promise}
   *   The promise of the api request.
   */
  public requestPasswordByUsername(username: string): Promise<any> {
    return this.authDriver.requestPasswordByUsername(username);
  }
}
