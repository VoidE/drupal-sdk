import { APIInterface } from '../API';
import { ConfigurationInterface } from '../Configuration';

export type InjectableProps = {
  api: APIInterface;
  config: ConfigurationInterface;
};
