import { APIInterface } from '../API';

export type ContextType = { [key: string]: any };
type HookArgs<T> = T & { context: ContextType };
type HookResult<T> = T & { context?: ContextType };

type BeforeRequestHookArgs = {
  api: APIInterface;
  requestOptions: { [key: string]: any };
};
export type BeforeRequestHook = (
  args: HookArgs<BeforeRequestHookArgs>,
) => Promise<HookResult<BeforeRequestHookArgs>>;

type AfterRequestHookArgs = {
  data: any;
  requestOptions: { [key: string]: any };
};
export type AfterRequestHook = (
  args: HookArgs<AfterRequestHookArgs>,
) => Promise<HookResult<AfterRequestHookArgs>>;

type BeforePathRequestHookArgs = {
  initialUrl: string;
  data: { [key: string]: any };
  params: { [key: string]: any };
};
export type BeforePathRequestHook = (
  args: HookArgs<BeforePathRequestHookArgs>,
) => Promise<HookResult<BeforePathRequestHookArgs>>;

export type Hooks = {
  beforeRequest?: BeforeRequestHook[];
  afterRequest?: AfterRequestHook[];
  beforePathRequest?: BeforePathRequestHook[];
};

export type HookNames = keyof Hooks;

type ValueOf<T> = T[keyof T];
export type AvailableHooks = ValueOf<Required<Hooks>>[number];
