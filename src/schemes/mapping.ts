export interface EntityMappingItemInterface {
  [key: string]: string | EntityMappingItemInterface;
}

export interface EntityMappingInterface {
  [bundle: string]: {
    [key: string]: string | EntityMappingItemInterface;
  };
}

export interface EntityMappingCollectionInterface {
  [entityType: string]: EntityMappingInterface;
}
