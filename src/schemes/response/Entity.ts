import { APIResponseInterface } from "../APIResponse";
import { EntityInterface } from "../drupal/Entity";
import { FieldInterface } from "../drupal/Field";

export interface EntityDataSetInterface extends EntityInterface {
  fields: {
    [field: string]: FieldInterface;
  };
}

export interface EntityResponseInterface extends APIResponseInterface<EntityDataSetInterface> {}

export interface EntitesDataSetInterface extends APIResponseInterface<EntityDataSetInterface[]> {}
