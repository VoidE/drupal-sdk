export interface AbstractAPIResponseInterface {
  error?: {
    code: number;
    message: string;
  };
}

export interface APIResponseInterface<
  DataType,
  MetaDataType extends object = {}
> extends AbstractAPIResponseInterface {
  meta: MetaDataType;
  data: DataType;
}

export interface APIMetaListInterface {
  resultCount: number;
  totalCount: number;
}
