export interface FilterInterface {
  [key: string]: any;
}
