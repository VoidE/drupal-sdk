import { TranslationInterface } from './Translation';

interface RequiredCollectionDataInterface {
  collection: string;
}

interface OptionalCollectionDataInterface {
  managed: boolean;
  hidden: boolean;
  single: boolean;
  icon: string;
  note: string;
  translation: TranslationInterface;
}

export interface EntityInterface
  extends RequiredCollectionDataInterface,
    Partial<OptionalCollectionDataInterface> {}
