import { TranslationInterface } from './Translation';

interface RequiredFieldDataInterface {
  field: string;
}

interface OptionalFieldDataInterface {
  unique: boolean;
  primary_key: boolean;
  auto_increment: boolean;
  default_value: any;
  note: string;
  signed: boolean;
  sort: number; // 0, 1?
  hidden_detail: boolean;
  hidden_browse: boolean;
  required: boolean;
  options: object;
  locked: boolean;
  translation: TranslationInterface;
  readonly: boolean;
  width: number;
  validation: string;
  group: number;
  length: number;
}

export interface FieldInterface
  extends RequiredFieldDataInterface,
    Partial<OptionalFieldDataInterface> {}
