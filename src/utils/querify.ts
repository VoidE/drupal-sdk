export type QuerifySerializer = (
  key: string,
  value: string | boolean | number,
) => string;

const defaultSerializeTransform: QuerifySerializer = (
  key: string,
  value: string | boolean | number,
) => `${key}=${encodeURIComponent(value)}`;

/**
 * Parse an object to an query string.
 *
 * @param {object} obj
 *   The object to parse.
 * @param {string} prefix?
 *   An optional prefix to use.
 * @param {QuerifySerializer} serializer
 *   The serializer to use.
 *
 * @return {string}
 *   The query string.
 */
export function querify(
  obj: { [index: string]: any },
  prefix?: string,
  serializer: QuerifySerializer = defaultSerializeTransform,
): string {
  const qs: string[] = [];

  Object.keys(obj).forEach((prop: string) => {
    const key = prefix ? `${prefix}[${prop}]` : prop;
    const val = obj[prop];

    qs.push(
      val !== null && typeof val === 'object'
        ? querify(val, key)
        : serializer(key, val),
    );
  });

  return qs.join('&');
}
