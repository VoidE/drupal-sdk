/**
 * @module utils
 */

/**
 * Global type checker.
 *
 * @param {string} t
 *  The type to check.
 * @param {any} v
 *   The input to check.
 *
 * @return {bool}
 *   Returns wheter the input matches the given type.
 */
const isType = (t: string, v: any) =>
  Object.prototype.toString.call(v) === `[object ${t}]`;

/**
 * Check wheter the input is an notnull.
 *
 * @param {any} v
 *   The input to check.
 *
 * @return {boolean}
 *   Returns if the input is an notnull or not.
 */
export const isNotNull = (v: any) => v !== null && v !== undefined;

/**
 * Check wheter the input is an string.
 *
 * @param {any} v
 *   The input to check.
 *
 * @return {boolean}
 *   Returns if the input is an string or not.
 */
export const isString = (v: any) => v && typeof v === 'string' && /\S/.test(v);

/**
 * Check wheter the input is a number.
 *
 * @param {any} v
 *   The input to check.
 *
 * @return {boolean}
 *   Returns if the input is a number or not.
 */
export const isNumber = (v: any) =>
  isType('Number', v) && Number.isFinite(v) && !Number.isNaN(parseFloat(v));

/**
 * Check wheter the input is a function.
 *
 * @param {any} v
 *   The input to check.
 *
 * @return {boolean}
 *   Returns if the input is a function or not.
 */
export const isFunction = (v: any) => v instanceof Function;

/**
 * Check wheter the input is an object or empty.
 *
 * @param {any} v
 *   The input to check.
 *
 * @return {boolean}
 *   Returns if the input is an object or empty or not.
 */
export const isObjectOrEmpty = (v: any) => isType('Object', v);

/**
 * Check wheter the input is an array or empty.
 *
 * @param {any} v
 *   The input to check.
 *
 * @return {boolean}
 *   Returns if the input is an array or empty or not.
 */
export const isArrayOrEmpty = (v: any) => isType('Array', v);

/**
 * Check wheter the input is an array.
 *
 * @param {any} v
 *   The input to check.
 *
 * @return {boolean}
 *   Returns if the input is an array or not.
 */
export const isArray = (v: any) => (!isArrayOrEmpty(v) ? false : v.length > 0);

/**
 * @param {any} v
 *   The input to check.
 *
 * @return {boolean}
 *   Returns wheter the input is an object or not.
 */
export const isObject = (v: any) => {
  if (!isObjectOrEmpty(v)) {
    return false;
  }

  return Object.prototype.toString.call(v) === '[object Object]';
};
