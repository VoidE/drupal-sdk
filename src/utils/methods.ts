import { InjectableProps } from '../schemes/injectableProps';
import { APIInterface } from '../API';

export type withContextMethod = (
  args: { [key: string]: any },
  { api, config }: InjectableProps,
) => Promise<{ api: APIInterface }>;
