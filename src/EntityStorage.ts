/* eslint-disable class-methods-use-this */
import {
  EntityMappingInterface,
  EntityMappingItemInterface,
} from './schemes/mapping';
import { ConfigurationInterface } from './Configuration';
import { APIInterface, RequestOptions } from './API';
import { QueryParams as QueryParamsType } from './schemes/http/Query';

interface EntityStorageInjectInterface {
  api: APIInterface;
  config: ConfigurationInterface;
  mapping: EntityMappingInterface;
  params?: QueryParamsType;
}

export class EntityStorage {
  public api: APIInterface;

  public config: ConfigurationInterface;

  public entityType: string;

  public bundle: string;

  public params: QueryParamsType;

  /**
   * Constructor of the EntityStorage class.
   *
   * @param {string} entityType
   *   The entity type to use.
   * @param {string|undefined} bundle
   *   The bundle to use.
   * @param {EntityStorageInjectInterface} inject
   *   Defined classes to inject.

   */
  constructor(
    entityType: string,
    bundle: string | undefined,
    { api, config, params = {} }: EntityStorageInjectInterface,
  ) {
    this.entityType = entityType;
    this.api = api;
    this.config = config;
    this.params = params;

    this.bundle = entityType;
    if (bundle !== undefined) {
      this.bundle = bundle;
    }
  }

  /**
   * Process the parameters of the request.
   *
   * @param {QueryParamsType} params
   *   The params to process
   *
   * @return {object}
   *   The processed parameters.
   */
  public processParams(params: QueryParamsType) {
    const requestParams = { ...this.params, ...params };
    if (!requestParams.bundle) {
      requestParams.bundle = this.bundle;
    }

    return requestParams;
  }

  /**
   * Request to read a single entity.
   *
   * @param {string} uuid
   *   The uuid of the entity to perform the request on.
   * @param {QueryParamsType} inputParams
   *   The query parameters to pass.
   * @param {object} options
   *   The extra options to add the request.
   *
   * @return {Promise}
   *   The promise of the request.
   */
  read(
    uuid: string,
    inputParams: QueryParamsType = {},
    options: RequestOptions = {},
  ) {
    const { bundle, ...params } = this.processParams(inputParams);

    return this.api.get(
      `${this.config.apiBasePath}/${this.entityType}/${bundle}/${uuid}`,
      params,
      options,
    );
  }

  /**
   * Request to read all entities.
   *
   * @param {QueryParamsType} inputParams
   *   The query parameters to pass.
   * @param {object} options
   *   The extra options to add the request.
   *
   * @return {Promise}
   *   The promise of the request.
   */
  readAll(inputParams: QueryParamsType = {}, options: RequestOptions = {}) {
    const { bundle, ...params } = this.processParams(inputParams);

    return this.api.get(
      `${this.config.apiBasePath}/${this.entityType}/${bundle}`,
      params,
      options,
    );
  }

  /**
   * Request to create a single entity.
   *
   * @param {object} body
   *   The body of the request.
   * @param {QueryParamsType} inputParams
   *   The query parameters to pass.
   * @param {object} options
   *   The extra options to add the request.
   *
   * @return {Promise}
   *   The promise of the request.
   */
  create(
    body: { [key: string]: any },
    inputParams: QueryParamsType = {},
    options: RequestOptions = {},
  ) {
    const { bundle, ...params } = this.processParams(inputParams);

    const data = {
      data: {
        type: `${this.entityType}--${bundle}`,
        ...body,
      },
    };

    return this.api.post(
      `${this.config.apiBasePath}/${this.entityType}/${bundle}`,
      data,
      params,
      options,
    );
  }

  /**
   * Request to update a single entity.
   *
   * @param {string} uuid
   *   The uuid of the entity to perform the request on.
   * @param {object} body
   *   The body of the request.
   * @param {QueryParamsType} inputParams
   *   The query parameters to pass.
   * @param {object} options
   *   The extra options to add the request.
   *
   * @return {Promise}
   *   The promise of the request.
   */
  update(
    uuid: string,
    body: {},
    inputParams: QueryParamsType = {},
    options: RequestOptions = {},
  ) {
    const { bundle, ...params } = this.processParams(inputParams);

    const data = {
      data: {
        id: uuid,
        type: `${this.entityType}--${bundle}`,
        ...body,
      },
    };

    return this.api.patch(
      `${this.config.apiBasePath}/${this.entityType}/${bundle}/${uuid}`,
      data,
      params,
      options,
    );
  }

  /**
   * Request to delete a single entity.
   *
   * @param {string} uuid
   *   The uuid of the entity to perform the request on.
   * @param {QueryParamsType} inputParams
   *   The query parameters to pass.
   * @param {object} options
   *   The extra options to add the request.
   *
   * @return {Promise}
   *   The promise of the request
   */
  delete(
    uuid: string,
    inputParams: QueryParamsType = {},
    options: RequestOptions = {},
  ) {
    const { bundle, ...params } = this.processParams(inputParams);
    return this.api.delete(
      `${this.config.apiBasePath}/${this.entityType}/${bundle}/${uuid}`,
      params,
      options,
    );
  }

  /**
   * Request to update a single entity.
   *
   * @param {string} fileData
   *   The fileData of the file to upload.
   * @param {string} fileName
   *   The fieldName of the file to upload.
   * @param {string} fieldName
   *   The field name of the entity.
   * @param {QueryParamsType} inputParams
   *   The query parameters to pass.
   * @param {object} options
   *   The extra options to add the request.
   *
   * @return {Promise}
   *   The promise of the request.
   */
  upload(
    fileData: any,
    fileName: string,
    fieldName: string,
    inputParams: QueryParamsType = {},
    options: RequestOptions = {},
  ) {
    const { bundle, ...params } = this.processParams(inputParams);

    const headers = {
      'Content-Type': 'application/octet-stream',
      Accept: 'application/vnd.api+json',
      'Content-Disposition': `file; filename="${fileName}"`,
    };

    return this.api.request(
      'post',
      `${this.config.apiBasePath}/${this.entityType}/${bundle}/${fieldName}`,
      params,
      fileData,
      headers,
      options,
    );
  }
}
