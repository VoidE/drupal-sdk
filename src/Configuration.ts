/**
 * @module Configuration
 */

import { AuthModes } from './Authentication';
import { APIStorageDriverInterface } from './APIStorage';
import { Hooks } from './schemes/hooks';

export interface ConfigurationInterface {
  token?: string;
  url?: string;
  authMode?: AuthModes;
  storage?: any;
  basicAuth?: {
    username: string;
    password: string;
  };
  storageKey?: any;
  ignoreSsl?: boolean;
  debug?: boolean;
  apiBasePath?: string;
  useDecoupledRouter: boolean;
  tokenExpirationTime: number;
  methods: {
    getRoute?: (...args: any[]) => any;
    loginByEmail?: (...args: any[]) => any;
    readByPath?: (...args: any[]) => any;
  };
  hooks: Hooks;
}

export interface ConfigurationOptionsInterface {
  url: string;
  token?: string;
  authMode?: AuthModes;
  tokenExpirationTime?: number;
  storage?: APIStorageDriverInterface;
  storageKey?: string;
  ignoreSsl?: boolean;
  debug?: boolean;
  apiBasePath?: string;
  basicAuth?: {
    username: string;
    password: string;
  };
  hooks?: Hooks;
  useDecoupledRouter?: boolean;
  methods?: ConfigurationInterface['methods'];
}

export interface ConfigurationDefaultsInterface {
  tokenExpirationTime: number;
  authMode: AuthModes;
  apiBasePath: string;
  storageKey: string;
  ignoreSsl: boolean;
  debug: boolean;
  useDecoupledRouter: boolean;
  methods: {};
  hooks: Hooks;
}

// configuration merged with defaults
export interface ConfigurationValuesInterface {
  url: string;
  token?: string;
  tokenExpirationTime: number;
  authMode: AuthModes;
  storage?: APIStorageDriverInterface;
  storageKey: string;
  ignoreSsl: boolean;
  debug: boolean;
  apiBasePath: string;
  useDecoupledRouter: boolean;
  methods: ConfigurationInterface['methods'];
  hooks: Hooks;
  basicAuth?: {
    username: string;
    password: string;
  };
}

export class Configuration implements ConfigurationInterface {
  public defaults: ConfigurationDefaultsInterface = {
    tokenExpirationTime: 5 * 6 * 1000,
    authMode: 'cookie',
    apiBasePath: '/jsonapi',
    storageKey: 'drupal-sdk',
    useDecoupledRouter: true,
    ignoreSsl: false,
    debug: false,
    methods: {},
    hooks: {
      beforeRequest: [],
      beforePathRequest: [],
    },
  };

  private internalConfiguration: ConfigurationValuesInterface;

  /**
   * Creates a new configuration instance, will be used once for each drupal instance (passing refs).
   *
   * @param {ConfigurationOptionsInterface} initialConfig
   *   Initial configuration values
   * @param {APIStorageDriverInterface?} storage
   *   Storage adapter for persistence
   */
  constructor(initialConfig: ConfigurationOptionsInterface = {} as any) {
    this.internalConfiguration = {
      ...this.defaults,
      ...initialConfig,
    };
  }

  /**
   * Get the authentication token.
   *
   * @return {string}
   *   The authentication token.
   */
  public get token(): string | undefined {
    return this.internalConfiguration.token;
  }

  /**
   * Sets the authentication token.
   *
   * @param  {string|undefined} token
   *   The authentication token
   */
  public set token(token: string | undefined) {
    this.partialUpdate({ token });
  }

  /**
   * Get the expiration time of the token.
   *
   * @return {number}
   *   The expiration time of the token.
   */
  public get tokenExpirationTime(): number {
    return this.internalConfiguration.tokenExpirationTime;
  }

  /**
   * Set the expiration time of the token.
   *
   * @param  {number|undefined} tokenExpirationTime
   *   The expiration time of the token.
   */
  public set tokenExpirationTime(tokenExpirationTime: number) {
    this.partialUpdate({ tokenExpirationTime });
  }

  /**
   * Get the url of the Drupal API.
   *
   * @return {string}
   *   Return the url of the Drupal API.
   */
  public get url(): string | undefined {
    return this.internalConfiguration.url;
  }

  /**
   * Get the url of the Drupal API.
   *
   * @param  {string|undefined} url
   *   The url of the Drupal API.
   */
  public set url(url: string | undefined) {
    this.partialUpdate({ url });
  }

  /**
   * Get the authentication mode.
   *
   * @return {AuthModes}
   *   The authentication mode.
   */
  public get authMode(): AuthModes {
    return this.internalConfiguration.authMode;
  }

  /**
   * Set the authentication mode.
   *
   * @param  {AuthModes} authMode
   *   The authentication mode.
   */
  public set authMode(authMode: AuthModes) {
    this.internalConfiguration.authMode = authMode;
  }

  /**
   * Get the api base path.
   *
   * @return {AuthModes}
   *   The api base path.
   */
  public get apiBasePath(): string {
    return this.internalConfiguration.apiBasePath;
  }

  /**
   * Set the api base path.
   *
   * @param  {string} apiBasePath
   *   The api base path.
   */
  public set apiBasePath(apiBasePath: string) {
    this.internalConfiguration.apiBasePath = apiBasePath;
  }

  /**
   * Get the authentication mode.
   *
   * @return {AuthModes}
   *   The authentication mode.
   */
  public get storage() {
    return this.internalConfiguration.storage;
  }

  /**
   * Get the storage key.
   *
   * @return {string}
   *   The storage key.
   */
  public get storageKey(): string {
    return this.internalConfiguration.storageKey;
  }

  /**
   * Set the storage key.
   *
   * @param  {string} storageKey
   *   The storage key.
   */
  public set storageKey(storageKey: string) {
    this.internalConfiguration.storageKey = storageKey;
  }

  /**
   * Get the method overrides.
   *
   * @return {object}
   *   The method overrides key.
   */
  public get methods(): {} {
    return this.internalConfiguration.methods;
  }

  /**
   * Get the hooks.
   *
   * @return {object}
   *   The hooks.
   */
  public get hooks(): Hooks {
    return this.internalConfiguration.hooks;
  }

  /**
   * Get the decoupledRouter option.
   *
   * @return {object| undefined}
   *   The decoupledRouter option.
   */
  public get basicAuth(): { username: string; password: string } | undefined {
    return this.internalConfiguration.basicAuth;
  }

  /**
   * Get the decoupledRouter option.
   *
   * @return {boolean}
   *   The decoupledRouter option.
   */
  public get useDecoupledRouter(): boolean {
    return this.internalConfiguration.useDecoupledRouter;
  }

  /**
   * Set the decoupledRouter option.
   *
   * @param  {boolean} useDecoupledRouter
   *   The decoupledRouter option.
   */
  public set useDecoupledRouter(useDecoupledRouter: boolean) {
    this.internalConfiguration.useDecoupledRouter = useDecoupledRouter;
  }

  /**
   * Get the ignoreSsl option.
   *
   * @return {boolean}
   *   The ignoreSsl option.
   */
  public get ignoreSsl(): boolean {
    return this.internalConfiguration.ignoreSsl;
  }

  /**
   * Set the ignoreSsl option.
   *
   * @param  {boolean} ignoreSsl
   *   The ignoreSsl option.
   */
  public set ignoreSsl(ignoreSsl: boolean) {
    this.internalConfiguration.ignoreSsl = ignoreSsl;
  }

  /**
   * Get the debug option.
   *
   * @return {boolean}
   *   The debug option.
   */
  public get debug(): boolean {
    return this.internalConfiguration.debug;
  }

  /**
   * Set the debug option.
   *
   * @param  {boolean} debug
   *   The debug option.
   */
  public set debug(debug: boolean) {
    this.internalConfiguration.debug = debug;
  }

  /**
   * Update the configuration values.
   *
   * @param {ConfigurationValuesInterface} config
   *   The config to update.
   */
  public update(config: ConfigurationValuesInterface): void {
    this.internalConfiguration = config;
  }

  /**
   * Update partials of the configuration, behaves like the [update] method
   *
   * @param {Partial<ConfigurationValuesInterface>} config
   *   The config to update.
   */
  public partialUpdate(config: Partial<ConfigurationValuesInterface>): void {
    const configuration = {
      ...this.internalConfiguration,
      ...config,
    };

    this.update(configuration);
  }
}
