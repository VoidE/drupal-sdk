/* eslint-disable class-methods-use-this */
/**
 * @module Connection
 */

import {
  Configuration,
  ConfigurationInterface,
  ConfigurationOptionsInterface,
} from './Configuration';
import { API, APIInterface, RequestOptions } from './API';
import { QueryParams as QueryParamsType } from './schemes/http/Query';
import { EntityStorage } from './EntityStorage';
import { APIStorage, APIStorageInterface } from './APIStorage';
import {
  EntityMappingCollectionInterface,
  EntityMappingItemInterface,
} from './schemes/mapping';

import {
  Authentication,
  AuthenticationInterface,
  LoginOptionsInterface,
  AuthenticationResponseInterface,
} from './Authentication';
import { BeforePathRequestHook, ContextType } from './schemes/hooks';

export class Connection {
  public configOptions: ConfigurationOptionsInterface;

  public config: ConfigurationInterface;

  public api: APIInterface;

  public auth: AuthenticationInterface;

  public storage: APIStorageInterface;

  public injectableProps: {};

  /**
   * @deprecated Mapping is deprecated since it's. Use hooks for mapping data.
   */
  public entityMapping: EntityMappingCollectionInterface;

  /**
   * Constructor for the Connection class.
   *
   * @param  {ConfigurationOptionsInterface} options
   *   The options to process.
   */
  constructor(options: ConfigurationOptionsInterface) {
    this.configOptions = options;
    this.config = new Configuration(options);
    this.api = new API(this.config);
    this.auth = new Authentication(this.config, { api: this.api });

    const { storageKey } = this.config;
    this.storage = new APIStorage(this.config.storage, storageKey);

    this.injectableProps = {
      config: this.config,
      api: this.api,
      auth: this.auth,
      storage: this.storage,
    };

    this.entityMapping = {};
  }

  /**
   * Get the Prepared API Object by entity type.
   *
   * @param  {string} mapPath
   *   The entity type.
   * @param  {MappingType} mapping
   *   The mapping for the entity.

   * @deprecated Mapping is deprecated since it's. Use hooks for mapping data.
   */
  public registerMapping(mapPath: string, mapping: EntityMappingItemInterface) {
    const [entityType, bundle] = mapPath.split('.');

    let finalBundle = bundle;
    if (bundle === undefined) {
      finalBundle = entityType;
    }

    if (this.entityMapping[entityType] === undefined) {
      this.entityMapping[entityType] = {};
    }
    this.entityMapping[entityType][finalBundle] = mapping;
  }

  /**
   * Get the Prepared API Object by entity type.
   *
   * @param  {string} entityType
   *   The entity type.
   * @param  {string|undefined} bundle
   *   The bundle to use.
   * @param  {QueryParamsType} params
   *   The query parameters to process.
   *
   * @return {EntityStorageInterface}
   *   The Prepared API Object.
   *
   */
  public get(
    entityType: string,
    bundle: string | undefined = undefined,
    params: QueryParamsType = {},
  ) {
    return new EntityStorage(entityType, bundle, {
      api: this.api,
      config: this.config,
      mapping: this.entityMapping[entityType],
      params,
    });
  }

  /**
   * Perform login request.
   *
   * @param  {string} username
   *   The username of the user.
   * @param  {string} password
   *   The password of the user.
   * @param  {LoginOptionsInterface} options?
   *   Optional options for the login request.
   *
   * @return {Promise}
   *   Return the request promise.
   */
  public login(
    username: string,
    password: string,
    options?: LoginOptionsInterface,
  ): Promise<AuthenticationResponseInterface> {
    return this.auth.login(username, password, options);
  }

  /**
   * Perform logout request.
   *
   * @return {Promise}
   *   Return the request promise.
   */
  public logout(): Promise<any> {
    return this.auth.logout();
  }

  /**
   * Request to read a single entity.
   *
   * @param {string} path
   *   The path of the entity to perform the request on.
   * @param {RequestOptions} options
   *   The options to pass to the request.
   *
   * @return {Promise}
   *   The promise of the request.
   *
   * @see: https://www.lullabot.com/articles/decoupled-hard-problems-routing
   */
  getRoute(path: string, options: RequestOptions = {}) {
    if (this.config.methods.getRoute !== undefined) {
      return this.config.methods.getRoute(path, this.injectableProps);
    }

    return this.api.get(
      `router/translate-path`,
      {
        path,
      },
      options,
    );
  }

  /**
   * Request to read a single entity.
   *
   * @param {string} path
   *   The path of the entity to perform the request on.
   * @param {QueryParamsType} inputParams
   *   The query parameters to pass.
   * @param {RequestOptions} options
   *   The options to pass to the request.
   *
   * @return {Promise}
   *   The promise of the request.
   *
   * @see: https://www.lullabot.com/articles/decoupled-hard-problems-routing
   */
  readByPath(
    path: string,
    inputParams: QueryParamsType = {},
    options: RequestOptions = {},
  ) {
    if (this.config.methods.readByPath !== undefined) {
      return this.config.methods.readByPath(
        path,
        inputParams,
        this.injectableProps,
      );
    }
    return this.getRoute(path, options).then(async (json: any) => {
      if (json.jsonapi !== undefined && json.jsonapi.individual !== undefined) {
        let processedData = json;
        let params = inputParams;

        const hookResult = await this.api.executeHook<BeforePathRequestHook>(
          'beforePathRequest',
          {
            initialUrl: path,
            data: processedData,
            params,
          },
        );
        processedData = hookResult.data;
        params = hookResult.params;

        return this.api.get(processedData.jsonapi.individual, params, options);
      }

      throw new Error('The path to the individual url could not be found.');
    });
  }

  /**
   * Get the current user object from the localstorage.
   *
   * @return {object|false}
   *   Returns the current user object from localstorage.
   */
  currentUser() {
    return this.storage.getItem('current_user') || false;
  }

  /**
   * Set the API storage after init.
   *
   * @param {APIStorageInterface} storage
   *   The storage type for the Drupal SDK.
   */
  setStorage(storage: APIStorageInterface) {
    const internalConfigOptions = { ...this.configOptions, storage };
    this.config = new Configuration(internalConfigOptions);
    this.api = new API(this.config);
    this.auth = new Authentication(this.config, { api: this.api });

    const { storageKey } = this.config;
    this.storage = new APIStorage(this.config.storage, storageKey);

    this.injectableProps = {
      config: this.config,
      api: this.api,
      auth: this.auth,
      storage: this.storage,
    };
  }

  /**
   * Set custom headers to every request.
   *
   * @param {ContextType} args
   *   The name of the header to set.

   * @return {Connection}
   *   The api object
   */
  withContext(args: ContextType) {
    this.api.setContext(args);

    this.injectableProps = {
      ...this.injectableProps,
      config: this.config,
      api: this.api,
      auth: this.auth,
    };

    return this;
  }
}
