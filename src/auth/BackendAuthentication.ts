/**
 * @module Authentication
 */

import {} from '../Authentication';
import { AuthenticationProviderInterface } from './BaseAuthentication';
import { CookieAuthentication } from './CookieAuthentication';

export class BackendAuthentication
  extends CookieAuthentication
  implements AuthenticationProviderInterface {
  /**
   * Perform login request.
   *
   * @param {string} username
   *   The username of the user.
   * @param {string} password
   *   The password of the user.
   *
   * @return {Promise}
   *   The promise of the api request.
   */
  public login(): Promise<any> {
    return this.api.get('/session/token', { _format: 'json' }).then((token) => {
      this.api.setCSRF(token);
      // this.storage.setItem('logout_token', token);
      return token;
    });
  }
}
