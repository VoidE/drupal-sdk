/* eslint-disable @typescript-eslint/no-unused-vars */
/**
 * @module Authentication
 */

import { APIInterface } from '../API';
import {
  AuthenticationResponseInterface,
  LoginBodyInterface,
} from '../Authentication';

export class UndefinedAuthentication {
  public api: APIInterface;

  private errorMessage: string;

  /**
   * Creates a new authentication instance.
   *
   * @param {APIInterface} api
   *   The API Class.
   */
  constructor(api: APIInterface) {
    this.api = api;
    this.errorMessage = 'No Authentication mode was provided.';
  }

  /**
   * Handling fake auth.
   *
   * @return {Promise}
   *   Returns a rejected promise
   */
  private handleAuth(): Promise<any> {
    return Promise.reject(new Error(this.errorMessage));
  }

  /**
   * Perform login request.
   *
   * @param {string} username
   *   The username of the user.
   * @param {string} password
   *   The password of the user.
   *
   * @return {Promise}
   *   The promise of the api request.
   */
  public login(
    username: string,
    password: string,
  ): Promise<AuthenticationResponseInterface> {
    return this.handleAuth();
  }

  // @todo Implement loginByEmail();
  // @todo Implement logout();
  // @todo Implement requestPassword();
  // @todo Implement requestPasswordByUsername();
}
