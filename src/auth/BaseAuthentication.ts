/**
 * @module Authentication
 */

import { APIInterface } from '../API';
import { APIStorageInterface } from '../APIStorage';

export interface AuthenticationProviderInterface {
  api: APIInterface;
  storage: APIStorageInterface;
  login(username: string, password: string): Promise<any>;
  logout(): Promise<any>;
  requestPassword(email: string): Promise<any>;
  requestPasswordByUsername(username: string): Promise<any>;
}

export class BaseAuthentication {
  public api: APIInterface;

  public storage: APIStorageInterface;

  /**
   * Creates a new authentication instance.
   *
   * @param {APIInterface} api
   *   The API Class.
   * @param {APIStorageInterface} storage
   *   The API Storage Class.
   */
  constructor(api: APIInterface, storage: APIStorageInterface) {
    this.api = api;
    this.storage = storage;
  }
}
