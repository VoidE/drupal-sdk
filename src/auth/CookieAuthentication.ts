/**
 * @module Authentication
 */

import {
  AuthenticationResponseInterface,
  LoginBodyInterface,
} from '../Authentication';
import {
  BaseAuthentication,
  AuthenticationProviderInterface,
} from './BaseAuthentication';

export class CookieAuthentication
  extends BaseAuthentication
  implements AuthenticationProviderInterface {
  /**
   * Perform login request.
   *
   * @param {string} username
   *   The username of the user.
   * @param {string} password
   *   The password of the user.
   *
   * @return {Promise}
   *   The promise of the api request.
   */
  public login(
    username: string,
    password: string,
  ): Promise<AuthenticationResponseInterface> {
    const body: LoginBodyInterface = {
      name: username,
      pass: password,
    };

    return this.api
      .post('/user/login', body, { _format: 'json' })
      .then((json: AuthenticationResponseInterface) => {
        this.api.setCSRF(json.csrf_token);
        this.storage.setItem('logout_token', json.logout_token);
        this.storage.setItem('current_user', json.current_user);
        return json;
      });
  }

  /**
   * Perform login request.
   *
   * @param {string} username
   *   The username of the user.
   * @param {string} password
   *   The password of the user.
   *
   * @return {Promise}
   *   The promise of the api request.
   */
  public logout(): Promise<any> {
    const logoutToken = this.storage.getItem('logout_token');
    return this.api
      .post('/user/logout', {}, { _format: 'json', token: logoutToken })
      .then((json: unknown) => {
        this.api.removeCSRF();
        this.storage.removeItem('logout_token');
        this.storage.removeItem('current_user');
        return json;
      });
  }

  /**
   * Perform password reset request.
   *
   * @param {string} email
   *   The email of the user.
   *
   * @return {Promise}
   *   The promise of the api request.
   */
  public requestPassword(email: string): Promise<any> {
    const body: {} = {
      mail: email,
    };

    return this.api.post('/user/password', body, {
      _format: 'json',
    });
  }

  /**
   * Perform password reset request by username.
   *
   * @param {string} username
   *   The username of the user.
   *
   * @return {Promise}
   *   The promise of the api request.
   */
  public requestPasswordByUsername(username: string): Promise<any> {
    const body: {} = {
      name: username,
    };

    return this.api.post('/user/password', body, {
      _format: 'json',
    });
  }
}
